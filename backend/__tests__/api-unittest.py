# -------

import unittest  # main, TestCase
import requests

class TestAPI(unittest.TestCase):
    # Endpoint for respective "get alls" tests checking for 200 status code
    def test_api_cities_get_all(self):
        self.assertEqual(requests.get("https://api.affordable-college-buddy.me/api/cities").status_code, 200, "Couldn't grab all cities")    
    def test_api_colleges_get_all(self):
        self.assertEqual(requests.get("https://api.affordable-college-buddy.me/api/colleges").status_code, 200, "Couldn't grab all colleges")
    def test_api_aids_get_all(self):
        self.assertEqual(requests.get("https://api.affordable-college-buddy.me/api/aid").status_code, 200, "Couldn't grab all aid")
    
    # Checking if the count parameter returns 200 status code
    def test_api_colleges_count(self):
        self.assertEqual(not requests.get("https://api.affordable-college-buddy.me/api/colleges", params={'count': '1'}).json()['data'], False, "colleges count didn't work")
    def test_api_cities_count(self):
        self.assertEqual(not requests.get("https://api.affordable-college-buddy.me/api/cities", params={'count': '1'}).json()['data'], False, "cities count didn't work")
    def test_api_aid_count(self):
        self.assertEqual(not requests.get("https://api.affordable-college-buddy.me/api/aid", params={'count': '1'}).json()['data'], False, "aid count didn't work")

    # Checking if the name parameter returns 200 status code
    def test_api_colleges_name(self):
        self.assertEqual(requests.get("https://api.affordable-college-buddy.me/api/colleges", params={'name': 'test'}).status_code, 200, "colleges name didn't work")
    def test_api_cities_name(self):
        self.assertEqual(requests.get("https://api.affordable-college-buddy.me/api/cities", params={'name': 'test'}).status_code, 200, "cities name didn't work")
    def test_api_aid_name(self):
        self.assertEqual(requests.get("https://api.affordable-college-buddy.me/api/aid", params={'name': 'test'}).status_code, 200, "aid name didn't work")

    # Checking if the id parameter returns 200 status code
    def test_api_colleges_id(self):
        self.assertEqual(requests.get("https://api.affordable-college-buddy.me/api/colleges", params={'id': '1'}).status_code, 200, "colleges id didn't work")
    def test_api_cities_id(self):
        self.assertEqual(requests.get("https://api.affordable-college-buddy.me/api/cities", params={'id': '1'}).status_code, 200, "cities id didn't work")
    def test_api_aid_id(self):
        self.assertEqual(requests.get("https://api.affordable-college-buddy.me/api/aid", params={'id': '1'}).status_code, 200, "aid id didn't work")

    # Checking linkage
    def test_linkage_1(self):
        self.assertEqual(requests.get("https://api.affordable-college-buddy.me/api/city_colleges", params={'id': 1}).status_code, 200, "city_college didn't work")
    def test_linkage_2(self):
        self.assertEqual(requests.get("https://api.affordable-college-buddy.me/api/college_scholarships", params={'scholarship_id': 1, 'college_id': 1}).status_code, 200, "college_scholarship didn't work")
    
    # Test the pages
    def test_page_college(self):
        self.assertEqual(requests.get("https://api.affordable-college-buddy.me/api/college_pag", params={'page': 1, 'per_page': 1}).status_code, 200, "college_pag didn't work")
    def test_page_cities(self):
        self.assertEqual(requests.get("https://api.affordable-college-buddy.me/api/city_pag", params={'page': 1, 'per_page': 1}).status_code, 200, "cpag didn't work")
    def test_page_aid(self):
        self.assertEqual(requests.get("https://api.affordable-college-buddy.me/api/aid_pag", params={'page': 1, 'per_page': 1}).status_code, 200, "aid_pag didn't work")

    # Checking if the search parameter returns 200 status code
    def test_search_college(self):
        self.assertEqual(requests.get("https://api.affordable-college-buddy.me/api/college_pag", params={'search': 'University'}).status_code, 200, "college_pag search didn't work")
    def test_search_cities(self):
        self.assertEqual(requests.get("https://api.affordable-college-buddy.me/api/city_pag", params={'search': 'Dallas'}).status_code, 200, "city_pag search didn't work")
    def test_search_aid(self):
        self.assertEqual(requests.get("https://api.affordable-college-buddy.me/api/aid_pag", params={'search': 'Scholarship'}).status_code, 200, "aid_pag search didn't work")
        
    # filter test params for college
    def test_filter_college_1(self):
        self.assertEqual(requests.get("https://api.affordable-college-buddy.me/api/college_pag?page=1&per_page=12&filter=1&filterDirection=>&filterNumber=200&sort=0&sortDirection=").json()["data"][0]["average_tuition_in_state"] > 200, True, "college_pag filter (average_tuition_in_state) didn't work or database problems")
    def test_filter_college_2(self):
        self.assertEqual(requests.get("https://api.affordable-college-buddy.me/api/college_pag?page=1&per_page=12&filter=2&filterDirection=>&filterNumber=200&sort=0&sortDirection=").json()["data"][0]["average_tuition_out_of_state"] > 200, True, "college_pag filter (average_tuition_out_of_state) didn't work or database problems")
    def test_filter_college_3(self):
        self.assertEqual(requests.get("https://api.affordable-college-buddy.me/api/college_pag?page=1&per_page=12&filter=3&filterDirection=>&filterNumber=0.01&sort=0&sortDirection=").json()["data"][0]["admission_rate"] > 0.01, True, "college_pag filter (admission_rate) didn't work or database problems")
    def test_filter_college_4(self):
        self.assertEqual(requests.get("https://api.affordable-college-buddy.me/api/college_pag?page=1&per_page=12&filter=4&filterDirection=>&filterNumber=200&sort=0&sortDirection=").json()["data"][0]["average_aid"] > 200, True, "college_pag filter (average_aid) didn't work or database problems")
    def test_filter_college_5(self):    
        self.assertEqual(requests.get("https://api.affordable-college-buddy.me/api/college_pag?page=1&per_page=12&filter=5&filterDirection=>&filterNumber=0.05&sort=0&sortDirection=").json()["data"][0]["aid_received_percentage"] > 0.05, True, "college_pag filter (aid_received_percentage) didn't work or database problems")

    # filter test params for city
    def test_filter_city_1(self):
        self.assertEqual(requests.get("https://api.affordable-college-buddy.me/api/city_pag?page=1&per_page=12&filter=1&filterDirection=>&filterNumber=20&sort=0&sortDirection=").json()["data"][0]["median_age"] > 20, True, "city_pag filter (median_age) didn't work or database problems")
    def test_filter_city_2(self):
        self.assertEqual(requests.get("https://api.affordable-college-buddy.me/api/city_pag?page=1&per_page=12&filter=2&filterDirection=>&filterNumber=200&sort=0&sortDirection=").json()["data"][0]["male_population"] > 200, True, "city_pag filter (male_population) didn't work or database problems")
    def test_filter_city_3(self):
        self.assertEqual(requests.get("https://api.affordable-college-buddy.me/api/city_pag?page=1&per_page=12&filter=3&filterDirection=>&filterNumber=200&sort=0&sortDirection=").json()["data"][0]["female_population"] > 200, True, "city_pag filter (female_population) didn't work or database problems")
    def test_filter_city_4(self):
        self.assertEqual(requests.get("https://api.affordable-college-buddy.me/api/city_pag?page=1&per_page=12&filter=4&filterDirection=>&filterNumber=200&sort=0&sortDirection=").json()["data"][0]["total_population"] > 200, True, "city_pag filter (total_population) didn't work or database problems")
    def test_filter_city_5(self):    
        self.assertEqual(requests.get("https://api.affordable-college-buddy.me/api/city_pag?page=1&per_page=12&filter=5&filterDirection=>&filterNumber=200&sort=0&sortDirection=").json()["data"][0]["number_of_veterans"] > 200, True, "city_pag filter (number_of_veterans) didn't work or database problems")

    def test_filter_aid_1(self):
        self.assertEqual(requests.get("https://api.affordable-college-buddy.me/api/aid_pag?page=1&per_page=12&filter=1&filterDirection=>&filterNumber=200&sort=0&sortDirection=").json()["data"][0]["award"] > 200, True, "aid_pag filter (award) didn't work or database problems")
    def test_filter_aid_2(self):
        self.assertEqual(requests.get("https://api.affordable-college-buddy.me/api/aid_pag?page=1&per_page=12&filter=2&filterDirection=>&filterNumber=2.0&sort=0&sortDirection=").json()["data"][0]["minimum_gpa"] > 2.0, True, "aid_pag filter (minimum_gpa) didn't work or database problems")
    def test_filter_aid_3(self):
        self.assertEqual(requests.get("https://api.affordable-college-buddy.me/api/aid_pag?page=1&per_page=12&filter=3&filterDirection==&filterNumber=Health education&sort=0&sortDirection=").json()["data"][0]["target_field_of_study"] == "Health education", True, "aid_pag filter (target_field_of_study) didn't work or database problems")
    def test_filter_aid_4(self):
        self.assertEqual(requests.get("https://api.affordable-college-buddy.me/api/aid_pag?page=1&per_page=12&filter=4&filterDirection==&filterNumber=TRUE&sort=0&sortDirection=").json()["data"][0]["need_based"] == True, True, "aid_pag filter (need_based) didn't work or database problems")
    def test_filter_aid_5(self):    
        self.assertEqual(requests.get("https://api.affordable-college-buddy.me/api/aid_pag?page=1&per_page=12&filter=5&filterDirection==&filterNumber=FALSE&sort=0&sortDirection=").json()["data"][0]["minority_focused"] == False, True, "aid_pag filter (minority_focused) didn't work or database problems")
    

if __name__ == '__main__':
    unittest.main()