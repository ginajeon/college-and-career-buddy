
# -------

import unittest  # main, TestCase
from supabase import create_client

supa_url = "https://yqxrcgggfzohzyjxndzf.supabase.co"
supa_key = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InlxeHJjZ2dnZnpvaHp5anhuZHpmIiwicm9sZSI6ImFub24iLCJpYXQiOjE2OTY4MTIzNjIsImV4cCI6MjAxMjM4ODM2Mn0.HTuWKXIM2yrsc6bVtec89TQwAvcyZk3Sbl4nYgEDUjc"

supabase = create_client(supa_url, supa_key)

class TestSupabaseCrud(unittest.TestCase):
    def test_supabase_request(self):
        # Cleaning database from test values
        supabase.table("colleges").delete().eq("name", "TEST_DATA").execute()
        supabase.table("cities").delete().eq("city", "TEST_DATA").execute()
        supabase.table("scholarships").delete().eq("name", "TEST_DATA").execute()
        supabase.table("colleges").delete().eq("id", 999999).execute()
        supabase.table("cities").delete().eq("id", 999999).execute()
        supabase.table("scholarships").delete().eq("id", 999999).execute()
        size_colleges = len(supabase.from_('colleges').select('*').execute().data)
        size_cities = len(supabase.from_('cities').select('*').execute().data)
        size_scholarships = len(supabase.from_('scholarships').select('*').execute().data)
        
        supabase.table("colleges").insert({"id":999999, "name":"TEST_DATA"}).execute()
        supabase.table("cities").insert({"id":999999, "city":"TEST_DATA"}).execute()
        supabase.table("scholarships").insert({"id":999999, "name":"TEST_DATA"}).execute()
        
        # Checking to see if new entries were added
        self.assertEqual(len(supabase.from_('colleges').select('*').execute().data), size_colleges + 1, "Error in creation for colleges")
        self.assertEqual(len(supabase.from_('cities').select('*').execute().data), size_cities + 1, "Error in creation for cities")
        self.assertEqual(len(supabase.from_('scholarships').select('*').execute().data), size_scholarships + 1, "Error in creation for scholarships")

        # Checking content of these entries
        test1 = supabase.from_('colleges').select('name').eq("name", "TEST_DATA").single().execute().data["name"]
        test2 = supabase.from_('cities').select('city').eq("city", "TEST_DATA").single().execute().data["city"]
        test3 = supabase.from_('scholarships').select('name').eq("name", "TEST_DATA").single().execute().data["name"]
        self.assertEqual(test1, "TEST_DATA", "Colleges weren't created properly")
        self.assertEqual(test2, "TEST_DATA", "Cities weren't created properly")
        self.assertEqual(test3, "TEST_DATA", "Scholarships weren't created properly")
        
        # Deleting test entries
        supabase.table("colleges").delete().eq("name", "TEST_DATA").execute()
        supabase.table("cities").delete().eq("city", "TEST_DATA").execute()
        supabase.table("scholarships").delete().eq("name", "TEST_DATA").execute()
        self.assertEqual(len(supabase.from_('colleges').select('*').execute().data), size_colleges, "Error in deletion for colleges")
        self.assertEqual(len(supabase.from_('cities').select('*').execute().data), size_cities, "Error in deletion for cities")
        self.assertEqual(len(supabase.from_('scholarships').select('*').execute().data), size_scholarships, "Error in deletion for scholarships")

if __name__ == '__main__':
    unittest.main()