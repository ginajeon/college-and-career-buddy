import requests
import os
from dotenv import load_dotenv
from supabase import create_client

load_dotenv()

google_key = os.environ.get("GOOGLE_API_KEY")
google_engine = os.environ.get("ENGINE_ID")
supa_url = os.environ.get("SUPABASE_URL")
supa_key = os.environ.get("SUPABASE_KEY")

# connect to our database
supabase = create_client(supa_url, supa_key)

def city_image_update():
    response = supabase.table('cities').select("*").execute()
    result = response.data

    # For each city in database, query google to get the first image url
    for i in range(len(result)):
        # Understan that creating these variable each run is inefficient, but didn't care
        search_query = result[i]['city'] + ", TX"
        url = 'https://www.googleapis.com/customsearch/v1'
        params = {
            'q': search_query,
            'key': google_key,
            'cx': google_engine,
            'searchType': 'image'
        }
        
        image_result = requests.get(url, params=params).json()
        if 'items' in image_result:
            curr_link = image_result['items'][0]['link']
            supabase.table('cities').update({'image_link': curr_link}).eq('city', result[i]['city']).execute()

def college_image_update():
    response = supabase.table('colleges').select("*").execute()
    result = response.data

    # For each college in database, query google to get the first image url
    for i in range(len(result)):
        # Understan that creating these variable each run is inefficient, but didn't care
        search_query = result[i]['name']
        url = 'https://www.googleapis.com/customsearch/v1'
        params = {
            'q': search_query,
            'key': google_key,
            'cx': google_engine,
            'searchType': 'image'
        }
        
        image_result = requests.get(url, params=params).json()
        if 'items' in image_result:
            curr_link = image_result['items'][0]['link']
            supabase.table('colleges').update({'image_link': curr_link}).eq('name', result[i]['name']).execute()


def scholarship_image_update():
    response = supabase.table('scholarships').select("*").execute()
    result = response.data

    # For each city in database, query google to get the first image url
    for i in range(len(result)):
        # Understan that creating these variable each run is inefficient, but didn't care
        search_query = result[i]['name'] + " scholarship"
        url = 'https://www.googleapis.com/customsearch/v1'
        params = {
            'q': search_query,
            'key': google_key,
            'cx': google_engine,
            'searchType': 'image'
        }
        
        image_result = requests.get(url, params=params).json()
        if 'items' in image_result:
            curr_link = image_result['items'][0]['link']
            supabase.table('scholarships').update({'image_link': curr_link}).eq('name', result[i]['name']).execute()

def get_default():
    #default image
    search_query = "money"
    url = 'https://www.googleapis.com/customsearch/v1'
    params = {
        'q': search_query,
        'key': google_key,
        'cx': google_engine,
        'searchType': 'image'
    }

    image_result = requests.get(url, params=params).json()
    if 'items' in image_result:
        curr_link = image_result['items'][0]['link']
        return (curr_link)
    
# Call our functions here 
# scholarship_image_update()