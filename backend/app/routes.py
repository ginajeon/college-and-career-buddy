from app import app
from sqlalchemy import create_engine, text
from flask import request, after_this_request
import json

engine = create_engine(
    "postgresql://postgres.yqxrcgggfzohzyjxndzf:jXukJ23gwUXbados@aws-0-us-east-1.pooler.supabase.com:6543/postgres"
)


@app.route("/api/colleges")
def colleges():
    with engine.connect() as connection:
        count = request.args.get("count")
        id = request.args.get("id")
        name = request.args.get("name")
        location = request.args.get("location")
        query = ""

        if count:
            query = text("SELECT COUNT(*) FROM colleges")
        elif id:
            query = text("SELECT * FROM colleges WHERE id = " + str(id))
        elif name:
            query = text("SELECT * FROM colleges WHERE name LIKE '%" + name + "%'")
        elif location:
            pass
        else:
            query = text("SELECT * FROM colleges")

        result = connection.execute(query)
        data = [x._asdict() for x in result.all()]
        return json.JSONEncoder().encode({"data": data})


@app.route("/api/aid")
def scholarships():
    with engine.connect() as connection:
        count = request.args.get("count")
        id = request.args.get("id")
        name = request.args.get("name")
        query = ""

        if count:
            query = text("SELECT COUNT(*) FROM scholarships")
        elif id:
            query = text("SELECT * FROM scholarships WHERE id = " + str(id))
        elif name:
            query = text("SELECT * FROM scholarships WHERE name LIKE '%" + name + "%'")
        else:
            query = text("SELECT * FROM scholarships")

        result = connection.execute(query)
        data = [x._asdict() for x in result.all()]
        return json.JSONEncoder().encode({"data": data})


@app.route("/api/cities")
def cities():
    with engine.connect() as connection:
        count = request.args.get("count")
        id = request.args.get("id")
        name = request.args.get("name")
        query = ""

        if count:
            query = text("SELECT COUNT(*) FROM cities")
        elif id:
            query = text("SELECT * FROM cities WHERE id = " + str(id))
        elif name:
            query = text("SELECT * FROM cities WHERE city LIKE '%" + name + "%'")
        else:
            query = text("SELECT * FROM cities")

        result = connection.execute(query)
        data = [x._asdict() for x in result.all()]
        return json.JSONEncoder().encode({"data": data})


@app.route("/api/city_colleges")
def city_colleges():
    with engine.connect() as connection:
        city_id = request.args.get("id")
        if not city_id:
            return json.JSONEncoder().encode({"data": []})
        result = connection.execute(
            text("SELECT * FROM colleges WHERE city_id = " + str(city_id))
        )
        data = [x._asdict() for x in result.all()]
        return json.JSONEncoder().encode({"data": data})


@app.route("/api/college_scholarships")
def college_scholarships():
    with engine.connect() as connection:
        scholarship_id = request.args.get("scholarship_id")
        college_id = request.args.get("college_id")
        if not (scholarship_id or college_id):
            return json.JSONEncoder().encode({"data": []})
        query = ""

        if scholarship_id:
            query = text(
                "SELECT * FROM college_scholarships WHERE scholarship_id = "
                + str(scholarship_id)
            )
        elif college_id:
            query = text(
                "SELECT * FROM college_scholarships WHERE college_id = "
                + str(college_id)
            )

        result = connection.execute(query)
        data = [x._asdict() for x in result.all()]
        return json.JSONEncoder().encode({"data": data})


@app.route("/api/college_pag")
def college_pag():
    with engine.connect() as connection:
        page = int(request.args.get("page", 1))
        per_page = int(request.args.get("per_page", 12))
        search = request.args.get("search")
        filterBy = request.args.get("filter")
        filterDirection = request.args.get("filterDirection")
        filterNumber = request.args.get("filterNumber")
        sort = request.args.get("sort")
        sortDirection = request.args.get("sortDirection")
        count = request.args.get("count")

        if not (page or per_page):
            return json.JSONEncoder().encode({"data": []})

        query = "SELECT * FROM colleges"
        search_text = ""

        if search:
            search_text = " | ".join(search.split())
            query += (
                " WHERE to_tsvector('english', COALESCE(name, '') || ' ' || COALESCE(CAST(average_tuition_in_state AS TEXT), '') || ' ' || COALESCE(CAST(average_tuition_out_of_state AS TEXT), '') || ' ' || COALESCE(CAST(admission_rate AS TEXT), '') || ' ' || COALESCE(CAST(minority_students AS TEXT), '') || ' ' || COALESCE(CAST(average_aid AS TEXT), '') || ' ' || COALESCE(CAST(aid_received_percentage AS TEXT), '') || ' ' || COALESCE(CAST(student_faculty_ratio AS TEXT), '')) @@ to_tsquery('english', '"
                + str(search_text)
                + ":*')"
            )

        if count and count.lower() == "true":
            if search_text:
                result = connection.execute(
                    text(
                        "SELECT COUNT(*) FROM colleges WHERE to_tsvector('english', COALESCE(name, '') || ' ' || COALESCE(CAST(average_tuition_in_state AS TEXT), '') || ' ' || COALESCE(CAST(average_tuition_out_of_state AS TEXT), '') || ' ' || COALESCE(CAST(admission_rate AS TEXT), '') || ' ' || COALESCE(CAST(minority_students AS TEXT), '') || ' ' || COALESCE(CAST(average_aid AS TEXT), '') || ' ' || COALESCE(CAST(aid_received_percentage AS TEXT), '') || ' ' || COALESCE(CAST(student_faculty_ratio AS TEXT), '')) @@ to_tsquery('english', '"
                        + str(search_text)
                        + ":*')"
                    )
                )
                data = [x._asdict() for x in result.all()]
                return json.JSONEncoder().encode({"data": data})
            else:
                query = "SELECT COUNT(*) FROM colleges"

        params = [
            "average_tuition_in_state",
            "average_tuition_out_of_state",
            "admission_rate",
            "average_aid",
            "aid_received_percentage",
        ]

        if filterBy and int(filterBy) > 0:
            try:
                query.index("WHERE")
                query += " AND "
            except:
                query += " WHERE "
            query += (
                params[int(filterBy) - 1] + " " + filterDirection + " " + filterNumber
            )

        if sort and int(sort) > 0:
            try:
                query.index("WHERE")
                query += " AND "
            except:
                query += " WHERE "
            query += params[int(sort) - 1] + " IS NOT NULL"
            query += " ORDER BY " + params[int(sort) - 1] + sortDirection

        offset = (page - 1) * per_page
        query += " LIMIT " + str(per_page) + " OFFSET " + str(offset)
        result = connection.execute(text(query))
        data = [x._asdict() for x in result.all()]
        return json.JSONEncoder().encode({"data": data})


@app.route("/api/city_pag")
def city_pag():
    with engine.connect() as connection:
        page = int(request.args.get("page", 1))
        per_page = int(request.args.get("per_page", 12))
        search = request.args.get("search")
        filterBy = request.args.get("filter")
        filterDirection = request.args.get("filterDirection")
        filterNumber = request.args.get("filterNumber")
        sort = request.args.get("sort")
        sortDirection = request.args.get("sortDirection")
        count = request.args.get("count")

        if not (page or per_page):
            return json.JSONEncoder().encode({"data": []})

        query = "SELECT * FROM cities"
        search_text = ""

        if search:
            search_text = " | ".join(search.split())
            query += (
                " WHERE to_tsvector('english', COALESCE(city, '') || ' ' || COALESCE(CAST(median_age AS TEXT), '') || ' ' || COALESCE(CAST(male_population AS TEXT), '') || ' ' || COALESCE(CAST(female_population AS TEXT), '') || ' ' || COALESCE(CAST(total_population AS TEXT), '') || ' ' || COALESCE(CAST(number_of_veterans AS TEXT), '') || ' ' || COALESCE(CAST(average_household_size AS TEXT), '') || ' ' || COALESCE(CAST(foreign_born AS TEXT), '') || ' ' || COALESCE(CAST(american_indian_alaskan_native_population AS TEXT), '') || ' ' || COALESCE(CAST(asian_population AS TEXT), '') || ' ' || COALESCE(CAST(black_african_american_population AS TEXT), '') || ' ' || COALESCE(CAST(hispanic_latino_population AS TEXT), '') || ' ' || COALESCE(CAST(white_population AS TEXT), '')) @@ to_tsquery('english', '"
                + str(search_text)
                + ":*')"
            )
        if count and count.lower() == "true":
            if search_text:
                result = connection.execute(
                    text(
                        "SELECT COUNT(*) FROM cities WHERE to_tsvector('english', COALESCE(city, '') || ' ' || COALESCE(CAST(median_age AS TEXT), '') || ' ' || COALESCE(CAST(male_population AS TEXT), '') || ' ' || COALESCE(CAST(female_population AS TEXT), '') || ' ' || COALESCE(CAST(total_population AS TEXT), '') || ' ' || COALESCE(CAST(number_of_veterans AS TEXT), '') || ' ' || COALESCE(CAST(average_household_size AS TEXT), '') || ' ' || COALESCE(CAST(foreign_born AS TEXT), '') || ' ' || COALESCE(CAST(american_indian_alaskan_native_population AS TEXT), '') || ' ' || COALESCE(CAST(asian_population AS TEXT), '') || ' ' || COALESCE(CAST(black_african_american_population AS TEXT), '') || ' ' || COALESCE(CAST(hispanic_latino_population AS TEXT), '') || ' ' || COALESCE(CAST(white_population AS TEXT), '')) @@ to_tsquery('english', '"
                        + str(search_text)
                        + "')"
                    )
                )
                data = [x._asdict() for x in result.all()]
                return json.JSONEncoder().encode({"data": data})
            else:
                query = "SELECT COUNT(*) FROM cities"
        params = [
            "median_age",
            "male_population",
            "female_population",
            "total_population",
            "number_of_veterans",
        ]
        if filterBy and int(filterBy) > 0:
            try:
                query.index("WHERE")
                query += " AND "
            except:
                query += " WHERE "
            query += (
                params[int(filterBy) - 1] + " " + filterDirection + " " + filterNumber
            )
        if sort and int(sort) > 0:
            try:
                query.index("WHERE")
                query += " AND "
            except:
                query += " WHERE "
            query += params[int(sort) - 1] + " IS NOT NULL"
            query += " ORDER BY " + params[int(sort) - 1] + sortDirection
        offset = (page - 1) * per_page
        query += " LIMIT " + str(per_page) + " OFFSET " + str(offset)
        result = connection.execute(text(query))
        data = [x._asdict() for x in result.all()]
        return json.JSONEncoder().encode({"data": data})


@app.route("/api/aid_pag")
def aid_pag():
    with engine.connect() as connection:
        page = int(request.args.get("page", 1))
        per_page = int(request.args.get("per_page", 12))
        search = request.args.get("search")
        filterBy = request.args.get("filter")
        filterDirection = request.args.get("filterDirection")
        filterNumber = request.args.get("filterNumber")
        sort = request.args.get("sort")
        sortDirection = request.args.get("sortDirection")
        count = request.args.get("count")
        if not (page or per_page):
            return json.JSONEncoder().encode({"data": []})

        query = "SELECT * FROM scholarships"
        search_text = ""

        if search:
            search_text = " | ".join(search.split())
            # award, minimum gpa, target field of study, need based, minority focused
            query += (
                " WHERE to_tsvector('english', COALESCE(name, '') || ' ' || COALESCE(CAST(award AS TEXT), '') || ' ' || COALESCE(CAST(minimum_gpa AS TEXT), '') || ' ' || COALESCE(target_field_of_study, '') || ' ' || COALESCE(CAST(need_based AS TEXT), '') || ' ' || COALESCE(CAST(minority_focused AS TEXT), '') || ' ' || COALESCE(CAST(description_or_quote AS TEXT), '') || ' ' || COALESCE(CAST(organization AS TEXT), '') || ' ' || COALESCE(CAST(deadline AS TEXT), '')) @@ to_tsquery('english', '"
                + str(search_text)
                + ":*')"
            )
        if count and count.lower() == "true":
            if search_text:
                result = connection.execute(
                    text(
                        "SELECT COUNT(*) FROM scholarships WHERE to_tsvector(name || ' ' || CAST(minimum_gpa AS TEXT) || ' ' || target_field_of_study || ' ' || CAST(need_based AS TEXT) || ' ' || CAST(minority_focused AS TEXT)) @@ to_tsquery('"
                        + str(search_text)
                        + "')"
                    )
                )
                data = [x._asdict() for x in result.all()]
                return json.JSONEncoder().encode({"data": data})
            else:
                query = "SELECT COUNT(*) FROM scholarships"

        params = [
            "award",
            "minimum_gpa",
            "target_field_of_study",
            "need_based",
            "minority_focused",
        ]
        if filterBy and int(filterBy) > 0:
            try:
                query.index("WHERE")
                query += " AND "
            except:
                query += " WHERE "
            query += params[int(filterBy) - 1] + " " + filterDirection
            if filterBy == "3":
                query += " " + "\'" + filterNumber + "\'"
            else:
                query += " " + filterNumber

        if sort and int(sort) > 0:
            try:
                query.index("WHERE")
                query += " AND "
            except:
                query += " WHERE "
            query += params[int(sort) - 1] + " IS NOT NULL"
            query += " ORDER BY " + params[int(sort) - 1] + sortDirection

        offset = (page - 1) * per_page
        query += " LIMIT " + str(per_page) + " OFFSET " + str(offset)
        result = connection.execute(text(query))
        data = [x._asdict() for x in result.all()]
        return json.JSONEncoder().encode({"data": data})
