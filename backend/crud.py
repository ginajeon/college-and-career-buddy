from dotenv import load_dotenv
import os
from flask import Flask
import requests
import json

load_dotenv()

from supabase import create_client

# Grab .env keys
url = os.environ.get("SUPABASE_URL")
key = os.environ.get("SUPABASE_KEY")

supabase = create_client(url, key)

# if len(supabase.from_('cities').select('*').eq('name', 'Austin').execute().data) > 0:
#     print("EXISTS, DO NOTHING")
# else :
#     id = supabase.table('cities').select('id').eq('name', 'test').single().execute().data['id']
#     supabase.table("cities").insert({"name":id}).execute()

# EXAMPLE CODE FOR ROW QUERYING
# response = supabase.from_('cities').select('*').eq('name', 'Austin').execute()
# exists = len(response.data) > 0
# if (exists):
#     print("WE FOUND IT")
# else:
#     print("DID NOT FIND")
# response = supabase.from_('cities').select('id').eq('name', 'test').execute()


# QUERYING FOR ID
# response = supabase.table('cities').select('id').eq('name', 'test').single().execute()
# print(response.data['id'])

page = 0

app = Flask(__name__)
@app.route('/')
def send_api_request():
    # Define the API endpoint
    api_endpoint = 'http://api.data.gov/ed/collegescorecard/v1/schools.json?api_key=HYXxLO1qcAsXb7uCMRJLYvnJYa5HU5xhGDwiJwYk&fields=latest.school.name,latest.school.city,latest.admissions.admission_rate.overall,latest.cost.tuition.in_state,latest.cost.tuition.out_of_state,latest.student.demographics.race_ethnicity.white,latest.student.demographics.student_faculty_ratio'
    
    # Send a GET request to the API endpoint with the parameters
    # response = requests.get(api_endpoint, params=payload)
    response = requests.get(api_endpoint, params={'page':page})

    if response.status_code == 200:
        # data = response.json()  # Return the response content as
        return response.json()
        return data
    else:
        return f"Failed to retrieve data. Status code: {response.status_code}"

# Since request comes in pages, we will loop for each page of requests
stopWhile = False
while (not stopWhile):
    # Data holds the api response, have to do [results] to get the actual response and not the metadata
    data = send_api_request()["results"]
    # currently only want 200 colleges (200 a page)
    if (len(data) == 0 or page > 10):
        stopWhile = True
    else:
        for i in range(len(data)):
            curr = data[i]
            # Grabbing and storing response variables
            temp_name = curr["latest.school.name"]
            temp_city = curr["latest.school.city"]
            temp_admission = curr["latest.admissions.admission_rate.overall"]
            temp_instate = curr["latest.cost.tuition.in_state"]
            temp_outstate = curr["latest.cost.tuition.out_of_state"]
            temp_demo = 1 - curr["latest.student.demographics.race_ethnicity.white"] if curr["latest.student.demographics.race_ethnicity.white"] else curr["latest.student.demographics.race_ethnicity.white"]
            temp_sf = curr["latest.student.demographics.student_faculty_ratio"]
            temp_id = 0
            
            
            if len(supabase.from_('colleges').select('*').eq('name', temp_name).execute().data) <= 0:
                # Connecting city to college
                # If city DNE, create a new row in cities
                if len(supabase.from_('cities').select('*').eq('name', temp_city).execute().data) <= 0:
                    supabase.table("cities").insert({"name":temp_city}).execute()
                    
                temp_id = supabase.table('cities').select('id').eq('name', temp_city).single().execute().data['id']
                    
                supabase.table("colleges").insert({
                    "name": temp_name,
                    "average_tuition_in_state": temp_instate,
                    "average_tuition_out_of_state": temp_outstate,
                    "admission_rate": temp_admission,
                    "minority_students": temp_demo,
                    "student_faculty_ratio": temp_sf,
                    "city_id": temp_id
                    }).execute()
    page += 1