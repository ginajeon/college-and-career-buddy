## Group Info

10am group 10: Affordable College Buddy

Community: Low-income / first-gen high school students in Austin

Our project is a website that serves as a tool for high school students in Austin to explore different college, financial aid/scholarship, and city options.

## Website
[affordable-college-buddy.me](https://www.affordable-college-buddy.me/)

## API Information

http://api.affordable-college-buddy.me/api/

https://documenter.getpostman.com/view/23724549/2s9YJW7SM8

## Team members
 - Gina Jeon (ginajeon@utexas.edu, @ginajeon)
 - Angel Sosa Adame (angelsosaadame@utexas.edu, @angelito-pdf)
 - Ayush Manoj (ayush.manoj@utexas.edu, @jonamhsuya)
 - Jonathan Ngo (Jonathantngo@utexas.edu, @JonathanTNgo)
 - Hima Tummalapalli (himatummalapalli@utexas.edu, @himatummalapalli1)

## Phase 1

Phase Leader: Gina

SHA: 00525e1a

- Gina: estimated-5hrs, actual-7hrs
- Angel: estimated-5hrs, actual-6hrs
- Ayush: estimated-5hrs, actual-7hrs
- Jonathan:estimated-5hrs, actual-6hrs
- Hima: estimated-5hrs, actual-7hrs

## Phase 2

Phase Leader: Ayush

SHA: 4225b7c1

- Gina: estimated-15hrs, actual-18hrs
- Angel:estimated-10hrs, actual-10hrs
- Ayush: estimated-10hrs, actual-15hrs
- Jonathan:estimate-10hrs, actual-8hrs
- Hima: estimated-10hrs, actual-13hrs

## Phase 3

Phase Leader: Hima

SHA: 24698950

- Gina: estimated-8hrs, actual-8hrs
- Angel: estimated-5hrs, actual-4 hrs
- Ayush: estimated-6hrs, actual-6hrs
- Jonathan: estimated-6hrs, actual-6hrs
- Hima: estimated-6hrs, actual-7hrs

## Leader Responsibilities
- Communicate with the rest of the group
- Make sure everything on the rubric gets done
