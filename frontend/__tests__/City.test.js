import { render, screen } from '@testing-library/react'
import City from '../src/app/components/City'
import '@testing-library/jest-dom'
import { describe } from 'node:test'

const mapurl = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d220449.196165526!2d-97.75574245!3d30.3077609!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8644b599a0cc032f%3A0x5d9b464bd469d57a!2sAustin%2C%20TX!5e0!3m2!1sen!2sus!4v1695589370057!5m2!1sen!2sus"

const colleges = [
    {
        id: 1,
        name: "Test University",
        path: "/colleges/rice"
    }
]

const scholarships2 = [
    {
        id: 1,
        scholarshipName: "test",
        path: "../../scholarships/terry-traditional-scholarship"
    },
    {
        id: 2,
        scholarshipName: "test1",
        path: "../../scholarships/scholarships-for-first-generation-students"
    }
  ]

// City Unit Tests
describe('City', () => {
    it ('renders an image', () => {
        render(<City width="100%" imagesrc="/austin.jpg" height = "300" cityName={"City"} stats={["stats"]} scholarships={scholarships2} 
                colleges={colleges} demographics={["demographics"]} mapsrc={mapurl}/>)

        const image = screen.getByRole('img')
        expect(image).toBeInTheDocument()    
    })

    it('renders a heading', () => {
        render(<City width="100%" imagesrc="/austin.jpg" height = "300" cityName={"City"} stats={["stats"]} scholarships={scholarships2} 
        colleges={colleges} demographics={["demographics"]} mapsrc={mapurl}/>)

        const cityNameHeading = screen.getByRole('heading', {name: "City"})

        expect(cityNameHeading).toBeInTheDocument()
    })

    it('renders a link to colleges', () => {
        render(<City width="100%" imagesrc="/austin.jpg" height = "300" cityName={"City"} stats={["stats"]} scholarships={scholarships2} 
        colleges={colleges} demographics={["demographics"]} mapsrc={mapurl}/>)

        const collegesLink = screen.getByRole('link', {name: "Test University"})

        expect(collegesLink).toBeInTheDocument()
    })

    it('renders a link to scholarships', () => {
        render(<City width="100%" imagesrc="/austin.jpg" height = "300" cityName={"City"} stats={["stats"]} scholarships={scholarships2} 
        colleges={colleges} demographics={["demographics"]} mapsrc={mapurl}/>)

        const scholarshipLink = screen.getByRole('link', {name: "test"})

        expect(scholarshipLink).toBeInTheDocument()
    })
})