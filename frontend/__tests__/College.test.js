import { render, screen } from '@testing-library/react'
import College from '../src/app/components/College'
import '@testing-library/jest-dom'
import { describe } from 'node:test'
  
const mapurl = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3445.3211256368504!2d-97.7366262591789!3d30.28491847490832!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8644b59b2584cfb7%3A0x8131ee4f174a21de!2sThe%20University%20of%20Texas%20at%20Austin!5e0!3m2!1sen!2sus!4v1695583986898!5m2!1sen!2sus"

const scholarships = [
{
    id: 1,
    scholarshipName: "test",
    url: "../../scholarships/terry-traditional-scholarship"
},
{
    id: 2,
    scholarshipName: "test1",
    url: "../../scholarships/scholarships-for-first-generation-students"
}
]
  
// College Unit Tests
describe('College', () => {
    it ('renders an image', () => {
        render(<College image="/ut.jpg" schoolName={"UT Austin"} stats={["%5"]} scholarships={scholarships} size="400" 
            mapsrc={mapurl} minority_students={["demographics"]} cityName={"Austin"} cityUrl={"/cities/austin"} />)

        const image = screen.getByRole('img')
        expect(image).toBeInTheDocument()    
    })

    it('renders a heading', () => {
        render(<College image="/ut.jpg" schoolName={"UT Austin"} stats={["%5"]} scholarships={scholarships} size="400" 
            mapsrc={mapurl} minority_students={["demographics"]} cityName={"Austin"} cityUrl={"/cities/austin"} />)

        const collegeHeading = screen.getByRole('heading', {name: "UT Austin"})

        expect(collegeHeading).toBeInTheDocument()
    })

    it('renders a link to cities', () => {
        render(<College image="/ut.jpg" schoolName={"UT Austin"} stats={["%5"]} scholarships={scholarships} size="400" 
            mapsrc={mapurl} minority_students={["demographics"]} cityName={"Austin"} cityUrl={"/cities/austin"} />)

        const citiesLink = screen.getByRole('link', {name: "Austin"})

        expect(citiesLink).toBeInTheDocument()
    })

    it('renders a link to scholarships', () => {
        render(<College image="/ut.jpg" schoolName={"UT Austin"} stats={["%5"]} scholarships={scholarships} size="400" 
            mapsrc={mapurl} minority_students={["demographics"]} cityName={"Austin"} cityUrl={"/cities/austin"} />)

        const scholarshipsLink = screen.getByRole('link', {id: 1})

        expect(scholarshipsLink).toBeInTheDocument()
    })
})
