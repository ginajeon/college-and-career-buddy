import { render, screen } from '@testing-library/react'
import SearchBar from '../src/app/components/SearchBar'
import '@testing-library/jest-dom'
import { describe } from 'node:test'

// SearchBar Unit Tests
describe('SearchBar', () => {
    // it ('renders input', () => {
    //   render(<SearchBar/>)
  
    //   const input = screen.getByPlaceholderText('Search for cities, schools, or scholarships...')
    //   expect(input).toBeInTheDocument()
    // })
  
    it('renders a button', () => {
      render(<SearchBar />)
   
      const button = screen.getByRole('button', {
        name: /Search/i,
      })
   
      expect(button).toBeInTheDocument()
    })
})
  
  
  