import { render, screen } from '@testing-library/react'
import Card from '../src/app/components/Card'
import Grid from '../src/app/components/Grid'
import '@testing-library/jest-dom'
import { describe } from 'node:test'

// https://www.youtube.com/watch?v=XTNqyEBPAFw&t=130s
// Card Unit Tests
describe('Card', () => {
  it ('renders an image', () => {
    render(<Card imgName={"/ut.jpg"} title={"UT"} text={"UT Austin"} instancePage={"/colleges/ut"} buttonText={"See More"}/>)

    const image = screen.getByRole('img')
    expect(image).toBeInTheDocument()
  })

  it ('renders a link', () => {
    render(<Card imgName={"/UT_logo.png"} title={"UT"} text={"UT Austin"} instancePage={"/colleges/ut"} buttonText={"See More"}/>)

    const link = screen.getByRole('link')
    expect(link).toBeInTheDocument()
  })
})

// const cards = [
//   {
//     id: 1,
//     imgName: "/ut.jpg", // from Wikimedia Commons
//     title: "Test Uni 1",
//     text:
//       "stats",
//     instancePage: "/colleges/ut",
//   },

//   {
//     id: 2,
//     imgName: "/rice.jpg", // from Wikimedia Commons
//     title: "Test Uni 2",
//     text:
//       "stats 2",
//     instancePage: "/colleges/rice",
//   },

//   {
//     id: 3,
//     imgName: "/a&m.jpg", // from iStock
//     title: "Test Uni 3",
//     text:
//       "stats 3",
//     instancePage: "/colleges/a&m",
//   },
// ];

// describe('Grid', () => {
//   it ('renders Cards', () => {
//     render(<Grid cards={cards}/>)

//     const card = screen.getByRole('Card')
//     expect(card).toBeInTheDocument()
//   })
// })