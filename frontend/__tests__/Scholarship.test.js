import { render, screen } from '@testing-library/react'
import Scholarship from '../src/app/components/Scholarship'
import '@testing-library/jest-dom'
import { describe } from 'node:test'

const colleges = [
    {
        id: 1,
        collegeName: "Test University",
        path: "/colleges/rice"
    }
]
const cities = [
    {
        id: 1,
        cityName: "City",
        path: "/cities/houston"
    }
]

// Scholarship Unit Tests
describe('Scholarship', () => {
    it ('renders an image', () => {
        render(<Scholarship image="/Rice_University_logo.jpg" scholarshipName={"scholarshipName"} stats={"stats"} 
        colleges={colleges} cities={cities} quote={"quote"} quoteSource={"Test University"}/>)

        const image = screen.getByRole('img')
        expect(image).toBeInTheDocument()    
    })

    it('renders a heading', () => {
        render(<Scholarship image="/Rice_University_logo.jpg" scholarshipName={"scholarshipName"} stats={"stats"} 
        colleges={colleges} cities={cities} quote={"quote"} quoteSource={"Test University"}/>)

        const scholarshipNameHeading = screen.getByRole('heading', {name: "scholarshipName"})

        expect(scholarshipNameHeading).toBeInTheDocument()
    })

    it('renders a link to colleges', () => {
        render(<Scholarship image="/Rice_University_logo.jpg" scholarshipName={"scholarshipName"} stats={"stats"} 
        colleges={colleges} cities={cities} quote={"quote"} quoteSource={"Test University"}/>)

        const collegesLink = screen.getByRole('link', {name: "Test University"})

        expect(collegesLink).toBeInTheDocument()
    })

    it('renders a link to cities', () => {
        render(<Scholarship image="/Rice_University_logo.jpg" scholarshipName={"scholarshipName"} stats={"stats"} 
        colleges={colleges} cities={cities} quote={"quote"} quoteSource={"Test University"}/>)

        const citiesLink = screen.getByRole('link', {name: "City"})

        expect(citiesLink).toBeInTheDocument()
    })
})

