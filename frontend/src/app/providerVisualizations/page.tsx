"use client"
import React, { useEffect, useState } from "react";
import * as d3 from "d3";

interface CityData {
    city: string;
    counties: string[];
    pantries: string[];
    population: number;
    poverty_rate: number;
    shelters: string[];
    state: string;
    zip_codes: string[];
}

interface ProcessedData {
    [key: string]: {
        count: number;
        totalPantries: number;
        averagePantries?: number;
        totalShelters: number;
        averageShelters?: number;
    };
}

export default function ProviderVisualizations() {
    const [data, setData] = useState<ProcessedData>({});

  useEffect(() => {
    const barData = [
      { label: "AL", value: 26 },
      { label: "AR", value: 25 },
      { label: "LA", value: 72 },
      { label: "MO", value: 1 },
      { label: "MS", value: 40 },
      { label: "TN", value: 13 },
    ];

    const barSvg = d3.select("#bar-chart-container")
      .append("svg")
      .attr("width", 500)
      .attr("height", 500);

    const xScale = d3.scaleBand()
      .domain(barData.map(d => d.label)) 
      .range([40, 400])
      .padding(0.1);

    const yScale = d3.scaleLinear()
      .domain([0, d3.max(barData, d => d.value) || 0])
      .range([200, 0]);

    barSvg.selectAll("rect")
      .data(barData)
      .enter()
      .append("rect")
      .attr("x", d => xScale(d.label) ?? 0)
      .attr("y", d => 40 + yScale(d.value))
      .attr("width", xScale.bandwidth())
      .attr("height", d => 200 - yScale(d.value)) 
      .attr("fill", "steelblue");

    barSvg.append("g")
      .attr("transform", "translate(0,240)")
      .call(d3.axisBottom(xScale))
      .selectAll("text")
      .attr("dy", "0.35em")
      .attr("transform", "rotate(45)")
      .style("text-anchor", "start");
      
    barSvg.append("g")
      .call(d3.axisLeft(yScale))
      .attr("transform", "translate(40,40)");

    barSvg.append("text")
      .attr("x", 240)
      .attr("y", 20)
      .attr("text-anchor", "middle")
      .style("font-size", "16px")
      .style("font-weight", "bold")
      .text("Number of Food Pantries Per State");
    barSvg.append("text")
      .attr("x", 240)
      .attr("y", 37)
      .attr("text-anchor", "middle")
      .style("font-size", "16px")
      .style("font-weight", "bold")
      .text("in Mississippi Delta Region");
  }, []);

  useEffect(() => {
    const buildVisualization = async () => {
      const finalData: { label: string; value: number }[] = [
        { label: "MO", value:0 },
        { label: "TN", value:0 },
        { label: "LA", value:45 },
        { label: "AL", value:0 },
        { label: "AR", value:47 },
        { label: "MS", value: 14 },
      ];

      const pie = d3.pie();
      const arcData = pie(finalData.map((entry) => entry.value));

      const arc = d3.arc()
        .innerRadius(0)
        .outerRadius(100);

      const svg = d3
        .select("#pie-chart-container")
        .append("svg")
        .attr("width", 700)
        .attr("height", 300);

      const g = svg.append("g").attr("transform", "translate(200,150)");

      g.selectAll("path")
        .data(arcData)
        .enter()
        .append("path")
        .attr("d", (d) => arc(d as unknown as d3.DefaultArcObject))
        .attr("fill", (d, i) => d3.schemeCategory10[i]);

      const legend = svg.append("g").attr("transform", "translate(320,55)");

      const legendRectSize = 18;
      const legendSpacing = 4;

      const legendItems = legend
        .selectAll(".legend-item")
        .data(finalData)
        .enter()
        .append("g")
        .attr("class", "legend-item")
        .attr(
          "transform",
          (d, i) => "translate(0," + i * (legendRectSize + legendSpacing) + ")"
        );

      legendItems
        .append("rect")
        .attr("width", legendRectSize)
        .attr("height", legendRectSize)
        .attr("fill", (d, i) => d3.schemeCategory10[i]);

      const total = finalData.reduce((acc, entry) => acc + entry.value, 0);
      legendItems
        .append("text")
        .attr("x", legendRectSize + legendSpacing)
        .attr("y", legendRectSize - legendSpacing)
        .text((d) => `(${d.label}, ${d.value})`);

      // title
      svg
        .append("text")
        .attr("x", 200)
        .attr("y", 20)
        .attr("text-anchor", "middle")
        .style("font-size", "16px")
        .style("font-weight", "bold")
        .text("Shelters per State in Mississippi Delta Region");
    };

    buildVisualization();
  });

  useEffect(() => {
    fetch('https://api.bridging-the-bayou.me/allCityBriefs')
        .then(response => response.json())
        .then((data: CityData[]) => {
            const processedData = processData(data);
            setData(processedData);
            createD3Chart(processedData, 'pantry-chart-container');
            createShelterChart(processedData, 'shelter-chart-container');
        });
}, []);

const processData = (apiData: CityData[]): ProcessedData => {
    let groupedData: ProcessedData = {};

    apiData.forEach(city => {
        const bracket = Math.floor(city.poverty_rate / 0.05) * 0.05;
        if (!groupedData[bracket]) {
            groupedData[bracket] = { count: 0, totalPantries: 0, totalShelters: 0 };
        }
        groupedData[bracket].count += 1;
        groupedData[bracket].totalPantries += city.pantries.length;
        groupedData[bracket].totalShelters += city.shelters.length;
    });

    Object.keys(groupedData).forEach(bracket => {
        const bracketData = groupedData[bracket];
        bracketData.averagePantries = bracketData.totalPantries / bracketData.count;
        bracketData.averageShelters = bracketData.totalShelters / bracketData.count;
    });

    return groupedData;
};

const createD3Chart = (processedData: ProcessedData, containerId: string) => {
// Set the dimensions and margins of the graph
const margin = { top: 20, right: 30, bottom: 40, left: 90 };
const width = 460 - margin.left - margin.right;
const height = 400 - margin.top - margin.bottom;

// Append the SVG object to the body of the page
const svg = d3.select(`#${containerId}`)
              .append("svg")
              .attr("width", width + margin.left + margin.right)
              .attr("height", height + margin.top + margin.bottom)
              .append("g")
              .attr("transform", `translate(${margin.left},${margin.top})`);

const maxAveragePantries = d3.max(
  Object.values(processedData)
  .map(d => d.averagePantries)
  .filter((pantryCount): pantryCount is number => pantryCount !== undefined)
);

// X axis: scale and draw
const x = d3.scaleLinear()
.domain([0, 0.5]) // Assuming your poverty rate goes up to a max of 0.5
.range([0, width]);

svg.append("g")
.attr("transform", `translate(0,${height})`)
.call(d3.axisBottom(x).tickValues(d3.range(0, 0.55, 0.05))); // Sets the ticks to increments of 0.05

const y = d3.scaleLinear()
.domain([0, 3]) // Assuming you want to show 1-8+ for number of food pantries
.range([height, 0]);

svg.append("g")
.call(d3.axisLeft(y).ticks(7));
const barWidth = width / Object.entries(processedData).length; // Calculate bar width based on the number of data points

// Create vertical bars
svg.selectAll("myRect")
.data(Object.entries(processedData))
.join("rect")
.attr("x", d => x(parseFloat(d[0])))
.attr("y", d => y(d[1].averagePantries || 0))
.attr("height", d => height - y(d[1].averagePantries || 0))
.attr("width", barWidth)
.attr("fill", "#69b3a2");

// Add labels to the axes
svg.append("text")
.attr("text-anchor", "end")
.attr("x", width)
.attr("y", height + margin.top + 20)
.text("Poverty Rate");

svg.append("text")
.attr("text-anchor", "end")
.attr("transform", "rotate(-90)")
.attr("y", -margin.left + 20)
.attr("x", -margin.top)
.text("Number of Food Pantries");
};

const createShelterChart = (processedData: ProcessedData, containerId: string) => {
     // Set the dimensions and margins of the graph
const margin = { top: 20, right: 30, bottom: 40, left: 90 };
const width = 460 - margin.left - margin.right;
const height = 400 - margin.top - margin.bottom;

// Append the SVG object to the body of the page
const svg = d3.select(`#${containerId}`)
              .append("svg")
              .attr("width", width + margin.left + margin.right)
              .attr("height", height + margin.top + margin.bottom)
              .append("g")
              .attr("transform", `translate(${margin.left},${margin.top})`);

const maxAveragePantries = d3.max(
  Object.values(processedData)
  .map(d => d.averagePantries)
  .filter((pantryCount): pantryCount is number => pantryCount !== undefined)
);

// X axis: scale and draw
const x = d3.scaleLinear()
.domain([0, 0.5]) // Assuming your poverty rate goes up to a max of 0.5
.range([0, width]);

svg.append("g")
.attr("transform", `translate(0,${height})`)
.call(d3.axisBottom(x).tickValues(d3.range(0, 0.55, 0.05))); // Sets the ticks to increments of 0.05

const y = d3.scaleLinear()
.domain([0, 3])
.range([height, 0]);

svg.append("g")
.call(d3.axisLeft(y).ticks(7));
const barWidth = width / Object.entries(processedData).length; // Calculate bar width based on the number of data points

// Create vertical bars
svg.selectAll("myRect")
.data(Object.entries(processedData))
.join("rect")
.attr("x", d => x(parseFloat(d[0])))
.attr("y", d => y(d[1].averageShelters || 0))
.attr("height", d => height - y(d[1].averageShelters || 0))
.attr("width", barWidth)
.attr("fill", "#69b3a2");

// Add labels to the axes
svg.append("text")
.attr("text-anchor", "end")
.attr("x", width)
.attr("y", height + margin.top + 20)
.text("Poverty Rate");

svg.append("text")
.attr("text-anchor", "end")
.attr("transform", "rotate(-90)")
.attr("y", -margin.left + 20)
.attr("x", -margin.top)
.text("Average Number of Sheters");
};

  return (
    <>
      <div id="bar-chart-container"></div>
      <div id="pie-chart-container"></div>
      <h2>Average Number of Food Pantries in Cities by Poverty Rate</h2>
        <div id="pantry-chart-container"></div>
        <h2>Average Number of Shelters in Cities by Poverty Rate</h2>
        <div id="shelter-chart-container"></div>
    </>
  );
}
