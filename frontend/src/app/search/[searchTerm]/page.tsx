"use client";
import React, { useState, useEffect } from "react";
import Grid from "../../components/Grid";

export default function Page({params, searchParams}: any) {
    const [highlight, setHighlight] = useState<string>("");
    const [cityCards, setCityCards] = useState<{ data: any[] }>({ data: [] });
    const [collegeCards, setCollegeCards] = useState<{ data: any[] }>({ data: [] });
    const [scholarshipCards, setScholarshipCards] = useState<{ data: any[] }>({ data: [] });
    const [currentPage, setCurrentPage] = useState<number>(1);
    const [showColleges, setShowColleges] = useState(true)
    const [showCities, setShowCities] = useState(false)
    const [showScholarships, setShowScholarships] = useState(false)
    const [citiesCount, setCitiesCount] = useState<number>(0);
    const [collegesCount, setCollegesCount] = useState<number>(0);
    const [scholarshipCount, setScholarshipCount] = useState<number>(0);
    const [currentModelCount, setCurrentModelCount] = useState<number>(0)
    const [numPagesToPass, setNumPagesToPass] = useState<number>(0);

    useEffect(() => {
        console.log("search term changed")
        if(params.searchTerm !== "") {
            const getCitiesData = async () => {
              const searchTermSplit = params.searchTerm.split("%20");
              let searchTermStr = searchTermSplit[0];
              console.log(typeof searchTermStr)
              for (let i = 1; i < searchTermSplit.length; i++) {
                searchTermStr += " " + searchTermSplit[i];
              }
              setHighlight(searchTermStr);
                const response = await fetch(
                `${process.env.API_URL}/api/city_pag?page=1&per_page=12&search=${params.searchTerm}&count=true`
                );
                const data = await response.json();
                console.log("search count " + data.data[0].count)
                setCitiesCount(data.data[0].count);
            };

          const getPaginatedDataCities = async () => {
            const response = await fetch(
              `${process.env.API_URL}/api/city_pag?page=1&per_page=12&search=${params.searchTerm}`
            );
            const data = await response.json();
            setCurrentPage(1);
            setCityCards(data);
            console.log(data);
          };
          
          const getCollegesData = async () => {
            const response = await fetch(
              `${process.env.API_URL}/api/college_pag?page=1&per_page=12&search=${params.searchTerm}&count=true`
            );
            const data = await response.json();
            console.log("search count " + data.data[0].count)
            setCollegesCount(data.data[0].count);
            setCurrentModelCount(data.data[0].count)
            setNumPagesToPass(data.data[0].count / 12)
            if (data.data[0].count < 12 && data.data[0].count > 0) {
                setNumPagesToPass(1)
            }
          };

          const getPaginatedDataColleges = async () => {
            const response = await fetch(
              `${process.env.API_URL}/api/college_pag?page=1&per_page=12&search=${params.searchTerm}`
            );
            const data = await response.json();
            setCurrentPage(1);
            setCollegeCards(data);
            console.log(data);
          };
    
          const getScholarshipsData = async () => {
            const response = await fetch(
              `${process.env.API_URL}/api/aid_pag?page=1&per_page=12&search=${params.searchTerm}&count=true`
            );
            const data = await response.json();
            console.log("search count " + data.data[0].count)
            setScholarshipCount(data.data[0].count);
          };

          const getPaginatedDataScholarships = async () => {
            const response = await fetch(
              `${process.env.API_URL}/api/aid_pag?page=1&per_page=12&search=${params.searchTerm}`
            );
            const data = await response.json();
            setCurrentPage(1);
            setScholarshipCards(data);
            console.log(data);
          };
          console.log(":here")
          getCitiesData();
          getPaginatedDataCities();
          getCollegesData();
          getPaginatedDataColleges();
          getScholarshipsData();
          getPaginatedDataScholarships();
        }
      }, [params.searchTerm]);

      var numPagesToPassCities = Math.ceil(citiesCount / 12);
      var numPagesToPassColleges= Math.ceil(collegesCount / 12);
      var numPagesToPassScholarships = Math.ceil(scholarshipCount / 12);

      const handleCollegeClick = () => {
        setShowColleges(true)
        setShowCities(false)
        setShowScholarships(false)
        setCurrentModelCount(collegesCount)
        setCurrentPage(1)
        setNumPagesToPass(numPagesToPassColleges)
      }

      const handleCitiesClick = () => {
        setShowColleges(false)
        setShowCities(true)
        setShowScholarships(false)
        setCurrentModelCount(citiesCount)
        setCurrentPage(1)
        setNumPagesToPass(numPagesToPassCities)
      }

      const handleScholarshipsClick = () => {
        setShowColleges(false)
        setShowCities(false)
        setShowScholarships(true)
        setCurrentModelCount(scholarshipCount)
        setCurrentPage(1)
        setNumPagesToPass(numPagesToPassScholarships)
      }

      const getNext = async () => {
        if (currentPage < numPagesToPass) {
            setCurrentPage((currentPage) => currentPage + 1);
        }
      };
    
      const getPrevious = async () => {
        if (currentPage > 1) {
            setCurrentPage((currentPage) => currentPage - 1);
        }

      };
    
      // Search Results Page Structure inspired by https://gitlab.com/rparappuram/cs373-idb-12/

    return(
        <>
            <div style={{ textAlign: "center", margin: "20px auto", maxWidth: "100%" }}>
                <p className="text-6xl font-bold my-12">Search Results</p>
            </div>
            <button
                onClick={handleCollegeClick}
                className="btn btn-primary"
                style={{ marginTop: "30px", marginLeft: "30px" }}
            >
                Colleges
            </button>
            <button
                onClick={handleCitiesClick}
                className="btn btn-primary"
                style={{ marginTop: "30px", marginLeft: "30px" }}
            >
                Cities
            </button>
            <button
                onClick={handleScholarshipsClick}
                className="btn btn-primary"
                style={{ marginTop: "30px", marginLeft: "30px" }}
            >
                Scholarships
            </button>

            {showCities && 
            <Grid
                cards={cityCards.data.map((city) => ({
                id: city.id,
                imgName: city.image_link,
                title: city.city + ", TX",
                text: `Median Age: ${city.median_age}\n Male Population: ${city.male_population}\n Female Population: ${city.female_population}\n Total Population: ${city.total_population}\n Number of Veterans: ${city.number_of_veterans}\n Average Household Size: ${city.average_household_size}`, // Add other properties as needed
                instancePage: `/cities/${city.id}`,
                }))}
                toHighlight={highlight}
            />}
            {showColleges && 
            <Grid
                cards={collegeCards.data.map((college) => ({
                id: college.id,
                imgName: college.image_link, // Replace 'imgUrl' with the actual property name from your API response
                title: college.name,
                text: `Average In-State Tuition: ${college.average_tuition_in_state}\n Average Out-of-State Tuition: ${college.average_tuition_out_of_state}\n Admission Rate: ${college.admission_rate}\n Average Aid: $${college.average_aid}\n Aid Received Percentage: ${college.aid_received_percentage}`, // Add other properties as needed
                instancePage: `/colleges/${college.id}`,
                }))}
                toHighlight={highlight}
            />}
            {showScholarships &&
            <Grid
                cards={scholarshipCards.data.map((scholarship) => ({
                id: scholarship.id,
                imgName: scholarship.image_link, // Replace 'imgUrl' with the actual property name from your API response
                title: scholarship.name,
                text: `Award: ${scholarship.award}\n Minimum GPA: ${scholarship.minimum_gpa}\n Target Field of Study: ${scholarship.target_field_of_study}\n Need Based? ${scholarship.need_based}\n Minority Focused? ${scholarship.minority_focused}`, // Add other properties as needed
                instancePage: `/scholarships/${scholarship.id}`,
                }))}
                toHighlight={highlight}
            />}
            <nav aria-label="Page Navigation">
            <ul className="pagination">
                <li className="page-item">
                <button className="page-link" onClick={getPrevious}>
                    Previous
                </button>
                </li>
                <li className="page-item">
                <button className="page-link">{String(currentPage) + " / " + String(numPagesToPass)}</button>
                </li>
                <li className="page-item">
                <button className="page-link" onClick={getNext}>
                    Next
                </button>
                </li>
            </ul>
            </nav>
            <p style={{ marginLeft: "20px" }}>
                {currentModelCount} Results Found
            </p>
      </>
    )
    
}
