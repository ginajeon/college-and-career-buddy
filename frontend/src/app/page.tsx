"use client";
import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.css";
import SearchBar from "./components/SearchBar";
import { useRouter } from "next/navigation"

export default function Home() {
  const backgroundImageUrl = "/college.jpg";
  const [searchTerm, setSearchTerm] = useState<string>("");
  const { push } = useRouter();
  // const backgroundImage = {
  //   backgroundImage: `url(${backgroundImageUrl})`,
  //   backgroundSize: "cover",
  //   backgroundRepeat: "no-repeat",
  //   backgroundPosition: "center",
  //   minHeight: "100vh", // Set minimum height to cover the entire viewport
  //   display: "flex",
  //   flexDirection: "column", // Center content vertically
  //   alignItems: "center",
  // };

    const handleSearch = (searchTerm: string) => {
      // Handle the search logic here with the searchTerm
      setSearchTerm(searchTerm)
      console.log("Search term:", searchTerm);
      // Source: https://stackoverflow.com/questions/58173809/next-js-redirect-from-to-another-page
      push(`/search/${searchTerm}`)
    };

  return (
    <>
      <div
        className="page-background-color"
        style={{
          backgroundImage: `url(${backgroundImageUrl})`,
          backgroundSize: "cover",
          backgroundRepeat: "no-repeat",
          backgroundPosition: "center",
          minHeight: "100vh", // Set minimum height to cover the entire viewport
          display: "flex",
          flexDirection: "column", // Center content vertically
          alignItems: "center",
        }}
      >
        <div className="row" style={{ marginTop: "30px" }}>
          <p className="text-center mt-16 mb-8 p-4 rounded-2xl border border-black font-bold bg-green-600 text-white text-7xl">
            Affordable College Buddy
          </p>
        </div>
        <SearchBar text="Search for colleges, scholarships, and cities..." onSearch={handleSearch}/>
        <div className="row mt-8">
          <a
            href='/colleges'
            className="btn btn-primary"
            style={{ marginTop: "30px" }}
          >
            Explore Colleges
          </a>
          <a
            href='/scholarships'
            className="btn btn-primary"
            style={{ marginTop: "30px" }}
          >
            Explore Scholarships
          </a>
          <a
            href='/cities'
            className="btn btn-primary"
            style={{ marginTop: "30px" }}
          >
            Explore Cities
          </a>
        </div>
      </div>
    </>
  );
}
