import React from "react";
import Link from "next/link";
import NavLink from "./NavLink";

const Navbar = () => {
  return (
    <>
      <div className="w-full h-20 flex flex-row justify-around items-center bg-green-800 sticky top-0">
        <NavLink href="/" text="Home" />
        <NavLink href="/colleges" text="Colleges" />
        <NavLink href="/scholarships" text="Scholarships" />
        <NavLink href="/cities" text="Cities" />
        <NavLink href="/about" text="About" />
        <NavLink href="/visualizations" text="Visualizations" />
        <NavLink href="/providerVisualizations" text="Provider Visualizations" />
        <NavLink href="https://documenter.getpostman.com/view/23724549/2s9YJW7SM8" text="API" />
      </div>
    </>
  );
};

export default Navbar;
