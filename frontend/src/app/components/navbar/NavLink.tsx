export default function NavLink(props: { href: string; text: string }) {
  return (
    <a
      href={props.href}
      target={props.href[0] !== "/" ? "_blank" : "_self"} // if external website, open in new tab
      className="bg-green-800 hover:bg-green-600 duration-200 w-1/6 h-full flex items-center justify-center text-white font-bold no-underline"
    >
      {props.text}
    </a>
  );
}
