import React, { useState } from "react";

const SearchBar = ({ text, onSearch }) => {
  const [searchTerm, setSearchTerm] = useState("");

  const handleInputChange = (event) => {
    setSearchTerm(event.target.value);
  };

  const handleSearchClick = () => {
    if (onSearch) {
      onSearch(searchTerm);
    }
  };

  return (
    <div className="flex flex-row w-[80vw] h-12 mx-auto" height="50px">
      <input
        type="text"
        placeholder={text}
        className="text-lg bg-neutral-200 w-full rounded-l-lg pl-4"
        value={searchTerm}
        onChange={handleInputChange}
        onKeyDown={(k) => {
          if (k.key === "Enter") handleSearchClick();
        }}
      />
      <button
        className="w-1/4 h-full rounded-r-lg bg-green-800 text-center text-white hover:bg-green-600 duration-200"
        onClick={handleSearchClick}
      >
        Search
      </button>
    </div>
  );
};

export default SearchBar;
