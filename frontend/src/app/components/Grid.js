import React from "react";
import Card from "./Card";

export default function Grid({ cards, toHighlight }) {
  return (
    <>
      <div className="flex flex-row flex-wrap justify-center gap-10 mt-4 px-8 py-16">
        {cards.map((card, index) => (
          <Card
            key={index}
            imgName={card.imgName}
            title={card.title}
            text={card.text}
            instancePage={card.instancePage}
            buttonText={card.buttonText}
            toHighlight={toHighlight}
          />
        ))}
      </div>
    </>
  );
}
