/* eslint-disable @next/next/no-img-element */
import React from "react";
import Image from "next/image";
// import { Highlight } from 'react-highlight-regex'
import Highlighter from 'react-highlight-words'

export default function Card({
  imgName,
  title,
  text,
  instancePage,
  buttonText,
  toHighlight
}) {

  const textArray = text.split("\n");

  // Referenced https://gitlab.com/alexandralewis1/cs373-idb-06/-/blob/main/frontend/src/components/ArenaCard.jsx?ref_type=heads
  const highlightText = (input) => {
    if (toHighlight != null && toHighlight != "" && toHighlight != []) {
      // return <Highlight text={input} match={textToHighlight} />
      return <Highlighter searchWords={toHighlight.split(" ")} autoEscape={true} textToHighlight={input} />
    }

    return input
  }
  return (
    <>
      <div className="flex flex-col items-center text-center gap-3 border rounded-xl shadow-md p-4 w-[25rem] h-min">
        <div>
          <img src={imgName} alt={title} className="object-cover h-64 w-64" />
        </div>
        <p className="font-bold">{highlightText(title)}</p>
        {textArray.map((item, index) => (
          <p key={index}>{highlightText(item)}</p>
        ))}
        <a
          href={instancePage}
          className="btn btn-primary"
          style={{ marginTop: "30px" }}
        >
          {buttonText !== undefined ? buttonText : "See More"}
        </a>
      </div>
    </>
  );
}
