/* eslint-disable @next/next/no-img-element */
import React from "react";
import Image from "next/image";

export default function City(props) {
  return (
    <div className="container mt-4">
      <div className="row mb-4" height="200px">
        <div className="col-12 p-0">
          <img
            className="banner-img"
            src={props.imagesrc}
            alt="Cropped Image"
            style={{ width: "100%", height: "300px", objectFit: "cover" }}
          />
        </div>
      </div>
      <div className="row mb-4" height="500px">
        <div className="col-6">
          <h1 style={{ fontSize: "45px", marginBottom: "10px" }}>
            {props.cityName}
          </h1>
          <br></br>
          {props?.stats?.map((stat, index) => (
            <h4 key={index} className="ut_h4">
              {stat}
            </h4>
          ))}
          <br></br>
          <h4 style={{ marginBottom: "13px" }}>Demographics:</h4>
          {props.demographics.map((demographic, index) => (
            <h4 key={index} className="ut_h4">
              {demographic}
            </h4>
          ))}
        </div>
        <div className="col-6">
          {/* <iframe src={props.mapsrc} width="100%" height="100%" style={{border: "0", marginLeft: "20px"}} allowFullScreen></iframe> */}
          <div
            style={{
              position: "relative",
              textAlign: "right",
              height: "500px",
              width: "600px",
            }}
          >
            <div
              style={{
                overflow: "hidden",
                background: "none!important",
                height: "500px",
                width: "600px",
              }}
            >
              <iframe
                width="600"
                height="500"
                style={{
                  overflow: "hidden",
                  background: "none!important",
                  height: "500px",
                  width: "600px",
                }}
                src={`https://maps.google.com/maps?q=${props.cityName},TX&t=&z=7&ie=UTF8&iwloc=&output=embed`}
                frameborder="0"
                scrolling="no"
                marginheight="0"
                marginwidth="0"
              ></iframe>
              <br />
              <a href="https://www.embedgooglemap.net"></a>
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-12">
          <h1
            style={{
              fontSize: "25px",
              marginTop: "30px",
              marginBottom: "15px",
            }}
          >
            Colleges in {props.cityName}:{" "}
          </h1>
          {props.colleges.map((college, index) => (
            <h4 key={index} className="btn btn-primary mr-5 mb-5">
              <a href={"/colleges/" + college.id}>{college.name}</a>
            </h4>
          ))}
          <h1
            style={{
              fontSize: "25px",
              marginTop: "30px",
              marginBottom: "15px",
            }}
          >
            Scholarships Offered:{" "}
          </h1>
          {props.scholarships.map((scholarship, index) => (
            <h4 key={index} className="btn btn-primary mr-5 mb-3">
              <a href={scholarship.path}>{scholarship.scholarshipName}</a>
            </h4>
          ))}
        </div>
      </div>
    </div>
  );
}
