import React from "react";
import Image from "next/image";

export default function College(props) {
  return (
    <div className="container mt-4">
      <div className="row mb-4">
        <div className="col-lg-5 col-md-7 col-sm-7 mb-4">
          <img
            src={props.image}
            alt="College Image"
            width={props.size}
            height={props.size}
          ></img>
        </div>
        <div className="col-sm-5 col-md-5 col-lg-7 mb-4">
          <h1 style={{ fontSize: "45px", marginBottom: "20px" }}>
            {props.schoolName}
          </h1>
          <h1
            style={{ fontSize: "20px", marginBottom: "10px" }}
            className="btn btn-primary"
          >
            <a href={props.cityUrl}>{props.cityName}</a>
          </h1>
          <br></br>
          {props.stats.map((stat, index) => (
            <h4 key={index} className="ut_h4">
              {stat}
            </h4>
          ))}
          <h4 style={{ marginBottom: "13px" }}>
            Minority Students: {props.minority_students}
          </h4>
          <h1
            style={{ fontSize: "20px", marginBottom: "10px" }}
            className="btn btn-primary"
          >
            <a href={props.url} target="_blank">
              Click to Learn More About This College!
            </a>
          </h1>
          <h1
            style={{
              fontSize: "25px",
              marginTop: "30px",
              marginBottom: "15px",
            }}
          >
            Scholarships Offered:
          </h1>
          {props.scholarships.map((scholarship, index) => (
            <h4 key={index} className="btn btn-primary mr-5 mb-5">
              <a href={scholarship.path}>{scholarship.scholarshipName}</a>
            </h4>
          ))}

          {/* <a
          href={instancePage}
          className="btn btn-primary"
          style={{ marginTop: "30px" }}
        >
          {buttonText !== undefined ? buttonText : "See More"}
        </a> */}
        </div>
      </div>
      {/* <div className="row mb-4">
        <iframe
          src={props.mapsrc}
          width="600"
          height="450"
          style={{ border: "0", marginLeft: "20px" }}
          allowFullScreen
        ></iframe>
      </div> */}
      <div
        style={{
          position: "relative",
          textAlign: "right",
          height: "500px",
          width: "600px",
        }}
      >
        <div
          style={{
            overflow: "hidden",
            background: "none!important",
            height: "500px",
            width: "600px",
          }}
        >
          <iframe
            width="600"
            height="500"
            style={{
              overflow: "hidden",
              background: "none!important",
              height: "500px",
              width: "600px",
            }}
            src={`https://maps.google.com/maps?q=${props.schoolName}${props.cityName}&t=&z=9&ie=UTF8&iwloc=&output=embed`}
            frameborder="0"
            scrolling="no"
            marginheight="0"
            marginwidth="0"
          ></iframe>
        </div>
      </div>
    </div>
  );
}
