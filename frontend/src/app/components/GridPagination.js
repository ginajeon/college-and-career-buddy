"use client"
import React from "react";
import { useState } from "react";
import { Pagination } from "react-bootstrap";

export default function GridPagination({numPages}) {
    const [activeNum, setActiveNum] = useState(1)
    const [currentPage, setCurrentPage] = useState(1)
    const [leftPage, setLeftPage] = useState(1)
    const [rightPage, setRightPage] = useState(3)
    const [middlePage, setMiddlePage] = useState(2)

    const getNext = () => setCurrentPage((currentPage == numPages) ? numPages : currentPage => currentPage + 1)
    const getPrevious = () => setCurrentPage((currentPage == 1) ? 1 : currentPage => currentPage - 1)

    return (
      <nav aria-label="Page navigation example">
        <ul class="pagination">
          <li class="page-item"><button class="page-link" onClick={getPrevious}>Previous</button></li>
          <li class="page-item"><button class="page-link" href="../colleges">{currentPage}</button></li>
          <li class="page-item"><button class="page-link" onClick={getNext}>Next</button></li>
        </ul>
      </nav>
   )
}