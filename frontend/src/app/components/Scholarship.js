import React from "react";
import Image from "next/image";

export default function Scholarship(props) {
  const stats = props.stats.split("\n");
  return (
    <div className="container mt-4">
      <div className="row mb-4">
        <div className="col-lg-5 col-md-7 col-sm-7 mb-4">
          <img
            src={props.image}
            alt="Scholarship Image"
            width={400}
            height={400}
          ></img>
        </div>
        <div className="col-sm-5 col-md-5 col-lg-7 mb-4">
          <h1 style={{ fontSize: "45px", marginBottom: "10px" }}>
            {props.scholarshipName}
          </h1>
          <br></br>
          <blockquote className="blockquote text-center mt-4 mb-2">
            <div className="big-quote text-left">“</div>
            <h2 className="mb-3">{props.quote}</h2>
            <footer className="blockquote-footer mt-2">
              {" "}
              {props.quoteSource}
            </footer>
          </blockquote>
          <br></br>
          {stats.map((stat, index) => (
            <h4 key={index} className="ut_h4">
              {stat}
            </h4>
          ))}
          <h1
            style={{
              fontSize: "25px",
              marginTop: "30px",
              marginBottom: "15px",
            }}
          >
            Colleges Offered At:{" "}
          </h1>
          {props.colleges.map((college, index) => (
            <h4 key={index} className="btn btn-primary mr-5 mb-3">
              <a
                href={college.path}
              >
                {college.collegeName}
              </a>
            </h4>
          ))}

          <h1
            style={{
              fontSize: "25px",
              marginTop: "30px",
              marginBottom: "15px",
            }}
          >
            Cities Offered In:{" "}
          </h1>
          {props.cities.map((city, index) => (
            <h4 key={index} className="btn btn-primary mr-5 mb-3">
              <a
                href={city.path}
              >
                {city.cityName}
              </a>
            </h4>
          ))}
        </div>
      </div>
    </div>
  );
}
