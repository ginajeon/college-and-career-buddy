import React from "react";
import Grid from "../components/Grid";
import getCommits from "../functions/getCommits";
import getIssues from "../functions/getIssues";

export default async function About() {
  const angelCommits = "# Commits: " + (await getCommits({ name: "Angel" }));
  const angelIssues = "# Issues: " + (await getIssues({ id: "13515646" }));
  const ginaCommits = "# Commits: " + (await getCommits({ name: "Gina" }));
  const ginaIssues = "# Issues: " + (await getIssues({ id: "15551338" }));
  const ayushCommits = "# Commits: " + (await getCommits({ name: "Ayush" }));
  const ayushIssues = "# Issues: " + (await getIssues({ id: "15541027" }));
  const jonathanCommits =
    "# Commits: " + (await getCommits({ name: "Jonathan Ngo" }));
  const jonathanIssues = "# Issues: " + (await getIssues({ id: "13540623" }));
  const himaCommits = "# Commits: " + (await getCommits({ name: "hima" }));
  const himaIssues = "# Issues: " + (await getIssues({ id: "15574346" }));

  const bioCards = [
    {
      id: 1,
      imgName: "/angel.jpeg",
      title: "Angel Sosa Adame (Frontend)",
      text:
        "Email: angelsosaadame@utexas.edu\nI'm a 4th year computer science student minoring in public communication of science. I'm from Denver and I have a cat named Olive Oil.\n" +
        angelCommits +
        "\n" +
        angelIssues +
        "\n" +
        "# Unit Tests: " +
        0,
      instancePage: "https://gitlab.com/angelito-pdf",
      buttonText: "GitLab Account",
    },

    {
      id: 2,
      imgName: "/ginaj.jpg",
      title: "Gina Jeon (Frontend)",
      text:
        "Email: ginajeon@utexas.edu\nI am a junior computer science major at The University of Texas at Austin. Outside of school, I enjoy cooking and practicing violin.\n" +
        ginaCommits +
        "\n" +
        ginaIssues +
        "\n" +
        "# Unit Tests: " +
        10,
      instancePage: "https://gitlab.com/ginajeon",
      buttonText: "GitLab Account",
    },

    {
      id: 3,
      imgName: "/ayush.jpeg",
      title: "Ayush Manoj (Backend)",
      text:
        "Email: ayush.manoj@utexas.edu\nI'm a sophomore at the University of Texas at Austin, studying computer science. In my free time, I like to hit the gym, explore different genres of music, watch movies and party with friends.\n" +
        ayushCommits +
        "\n" +
        ayushIssues +
        "\n" +
        "# Unit Tests: " +
        0,
      instancePage: "https://gitlab.com/jonamhsuya",
      buttonText: "GitLab Account",
    },
    {
      id: 4,
      imgName: "/jonathan.jpeg",
      title: "Jonathan Ngo (Backend)",
      text:
        "Email: Jonathantngo@utexas.edu\n5th year CS major and Geosciences minor with 2 cats!\n" +
        jonathanCommits +
        "\n" +
        jonathanIssues +
        "\n" +
        "# Unit Tests: " +
        36,
      instancePage: "https://gitlab.com/JonathanTNgo",
      buttonText: "GitLab Account",
    },
    {
      id: 5,
      imgName: "/hima.jpeg",
      title: "Hima Tummalapalli (Frontend)",
      text:
        "Email: himatummalapalli@utexas.edu\nI'm a junior studying computer science. In my free time, I like to play table tennis and go for walks.\n" +
        himaCommits +
        "\n" +
        himaIssues +
        "\n" +
        "# Unit Tests: " +
        13,
      instancePage: "https://gitlab.com/himatummalapalli1",
      buttonText: "GitLab Account",
    },
  ];
  const toolCards = [
    {
      id: 1,
      imgName: "/next_logo.webp",
      title: "Next.js",
      text: "",
      instancePage: "https://nextjs.org/",
      buttonText: "Learn More",
    },

    {
      id: 2,
      imgName: "/postman.jpeg",
      title: "Postman",
      text: "",
      instancePage: "https://www.postman.com/",
      buttonText: "Learn More",
    },

    {
      id: 3,
      imgName: "/ed_disc.webp",
      title: "Ed Discussion",
      text: "",
      instancePage: "https://edstem.org",
      buttonText: "Learn More",
    },
    {
      id: 4,
      imgName: "/bootstrap.png",
      title: "Bootstrap",
      text: "",
      instancePage: "https://getbootstrap.com/",
      buttonText: "Learn More",
    },
    {
      id: 5,
      imgName: "/react-bootstrap.png",
      title: "React Bootstrap",
      text: "",
      instancePage: "https://react-bootstrap.netlify.app/",
      buttonText: "Learn More",
    },
    {
      id: 6,
      imgName: "/namecheap.png",
      title: "Namecheap",
      text: "",
      instancePage: "https://www.namecheap.com/",
      buttonText: "Learn More",
    },
    {
      id: 7,
      imgName: "/vscode.png",
      title: "VSCode",
      text: "",
      instancePage: "https://code.visualstudio.com/",
      buttonText: "Learn More",
    },
    {
      id: 8,
      imgName: "/amplify.png",
      title: "AWS Amplify",
      text: "",
      instancePage: "https://aws.amazon.com/amplify/",
      buttonText: "Learn More",
    },
    {
      id: 9,
      imgName: "/gitlab.png",
      title: "GitLab API",
      text: "",
      instancePage: "https://docs.gitlab.com/ee/api/",
      buttonText: "Learn More",
    },
    {
      id: 10,
      imgName: "/gitlab.png",
      title: "Flask",
      text: "",
      instancePage: "https://flask.palletsprojects.com/en/3.0.x/",
      buttonText: "Learn More",
    },
    {
      id: 11,
      imgName: "/jest.png",
      title: "Jest",
      text: "",
      instancePage: "https://jestjs.io/",
      buttonText: "Learn More",
    },
    {
      id: 12,
      imgName: "/selenium.png",
      title: "Selenium",
      text: "",
      instancePage: "https://www.selenium.dev/",
      buttonText: "Learn More",
    },
    {
      id: 13,
      imgName: "/supabase.jpeg",
      title: "Supabase",
      text: "",
      instancePage: "https://supabase.com/",
      buttonText: "Learn More",
    },
    {
      id: 14,
      imgName: "/ec2.png",
      title: "AWS EC2",
      text: "",
      instancePage: "https://aws.amazon.com/ec2/",
      buttonText: "Learn More",
    },
    {
      id: 15,
      imgName: "/slack.png",
      title: "Slack",
      text: "",
      instancePage: "https://slack.com/",
      buttonText: "Learn More",
    },
    
  ];
  const dataSourceCards = [
    {
      id: 1,
      imgName: "/dep_edu.png",
      title: "College Scorecard API",
      text: "",
      instancePage: "https://collegescorecard.ed.gov/data/",
      buttonText: "Learn More",
    },

    {
      id: 2,
      imgName: "/opendatasoft.jpeg",
      title: "opendatasoft US Cities: Demographics",
      text: "",
      instancePage: "https://public.opendatasoft.com/explore/dataset/us-cities-demographics/api/",
      buttonText: "Learn More",
    },

    {
      id: 3,
      imgName: "/careeronestop.jpeg",
      title: "careeronestop Scholarship Finder",
      text: "",
      instancePage: "https://www.careeronestop.org/Toolkit/Training/find-scholarships.aspx",
      buttonText: "Learn More",
    },
  ];
  return (
    <div className="flex flex-col gap-6 items-center text-center">
      <p className="text-6xl font-bold mt-12">About</p>
      <p className="mx-[20%]">
        Affordable College Buddy was created to support low-income high school
        students in Austin, TX who don&apos;t have access to college counseling.
        We provide students with information on affordable colleges close to
        their homes, scholarship options to fund their education, and
        information about the different cities.
      </p>
      <div className="my-12">
        <p style={{ fontSize: "2rem" }}>Team</p>
        <Grid cards={bioCards} toHighlight={null}/>
      </div>
      <div className="my-12">
        <p style={{ fontSize: "2rem" }}>Tools Used</p>
        <Grid cards={toolCards} toHighlight={null}/>
      </div>
      <div className="my-12">
        <p style={{ fontSize: "2rem" }}>Data Sources</p>
        <Grid cards={dataSourceCards} toHighlight={null}/>
      </div>
      <div className="my-12">
        <p style={{ fontSize: "2rem" }}>
          Interesting Result of Integrating Disparate Data
        </p>
        <p className="mx-[20%] mt-3">
          We found that integrating the three disparate data sources allows for a more comprehensive
          view for a student to know what their college education could look like. Our platform serves
          as a centralized hub where users can find out about a certain college, what type of city it’s
          located in, and how they can fund their potential future. A student coming from an economically
          disadvantaged background might think that their options for college are few and far between, 
          but our website equips them with the resources to discover how accessible and affordable higher
          education can be. 
        </p>
      </div>
    </div>
  );
}
