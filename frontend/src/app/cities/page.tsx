"use client";
import Grid from "../components/Grid";
import SearchBar from "../components/SearchBar";
import React, { useState, useEffect } from "react";

export default function Cities() {
  const [citiesCount, setCitiesCount] = useState<number>(0);
  const [cards, setCards] = useState<{ data: any[] }>({ data: [] });
  const [currentPage, setCurrentPage] = useState(1);
  const [loading, setLoading] = useState<boolean>(true);
  const [searchTerm, setSearchTerm] = useState<string>("");
  const [filter, setFilter] = useState(0);
  const [filterDirection, setFilterDirection] = useState("=");
  const [filterNumber, setFilterNumber] = useState("");
  const [filterRequest, setFilterRequest] = useState(0);
  const [sort, setSort] = useState(0);
  const [sortDirection, setSortDirection] = useState("");
  const [sortRequest, setSortRequest] = useState(0);
  // referenced https://gitlab.com/jrothfus/group-10-idb/-/blob/main/frontend/src/views/Search.jsx?ref_type=heads to do highlighting
  const [highlight, setHighlight] = useState("");

  useEffect(() => {
    setLoading(true);
    const getData = async () => {
      const response = await fetch(
        `${process.env.API_URL}/api/cities?count=true`
      );
      const data = await response.json();
      setCitiesCount(data.data[0].count);
    };

    const getPaginatedData = async () => {
      const response = await fetch(
        `${process.env.API_URL}/api/city_pag?page=1&per_page=12`
      );
      const data = await response.json();
      setCards(data);
      console.log(data);
    };

    getData();
    getPaginatedData();
    setLoading(false);
  }, []);

  // for searching
  useEffect(() => {
    console.log("search term changed");
    if (searchTerm === "") {
      setHighlight("");
      const getData = async () => {
        const response = await fetch(
          `${process.env.API_URL}/api/city_pag?page=${currentPage}&per_page=12&filter=${filter}&filterDirection=${filterDirection}&filterNumber=${filterNumber}&count=true`
        );
        const data = await response.json();
        if (data.data[0] != null) {
          console.log("search count " + data.data[0].count);
          setCitiesCount(data.data[0].count);
        }
      };

      const getPaginatedData = async () => {
        const response = await fetch(
          `${process.env.API_URL}/api/city_pag?page=${currentPage}&per_page=12&filter=${filter}&filterDirection=${filterDirection}&filterNumber=${filterNumber}&sort=${sort}&sortDirection=${sortDirection}`
        );
        const data = await response.json();
        setCards(data);
        console.log(data);
      };
      getData();
      getPaginatedData();
    } else {
      const getData = async () => {
        const response = await fetch(
          `${process.env.API_URL}/api/city_pag?page=${currentPage}&per_page=12&search=${searchTerm}&count=true`
        );
        const data = await response.json();
        console.log("search count " + data.data[0].count);
        setCitiesCount(data.data[0].count);
      };

      const getPaginatedData = async () => {
        setHighlight(searchTerm);
        const response = await fetch(
          `${process.env.API_URL}/api/city_pag?page=${currentPage}&per_page=12&search=${searchTerm}&filter=${filter}&filterDirection=${filterDirection}&filterNumber=${filterNumber}&sort=${sort}&sortDirection=${sortDirection}`
        );
        const data = await response.json();
        setCards(data);
        console.log(data);
      };
      getData();
      getPaginatedData();
    }
  }, [currentPage, searchTerm, filterRequest, sortRequest]);

  var numPagesToPass = Math.ceil(citiesCount / 12);

  const getNext = async () => {
    if (currentPage < numPagesToPass) {
      setCurrentPage((currentPage) => currentPage + 1);
    }
  };

  const getPrevious = async () => {
    if (currentPage > 1) {
      setCurrentPage((currentPage) => currentPage - 1);
    }
  };

  if (loading) return <p className="m-5">Loading...</p>;

  const handleSearch = async (newSearchTerm: string) => {
    // Handle the search logic here with the searchTerm
    console.log("Search term:", newSearchTerm);
    setCurrentPage(1);
    setFilter(0);
    setSort(0);
    setSearchTerm(newSearchTerm);
  };

  return (
    <>
      <div
        style={{ textAlign: "center", margin: "20px auto", maxWidth: "100%" }}
      >
        <p className="text-6xl font-bold my-12">Cities</p>
      </div>
      <SearchBar text="Search for cities..." onSearch={handleSearch} />
      <div className="flex flex-wrap items-center justify-around gap-4 my-12 px-8">
        <div className="flex flex-row items-center gap-2">
          <p className="font-bold">Filter by:</p>
          <select
            className="form-select w-72"
            value={filter}
            onChange={(v) => setFilter(v.target.value as unknown as number)}
          >
            <option value={0}>Select...</option>
            <option value={1}>Median Age</option>
            <option value={2}>Male Population</option>
            <option value={3}>Female Population</option>
            <option value={4}>Total Population</option>
            <option value={5}>Number of Veterans</option>
          </select>
          <select
            className="form-select w-16"
            value={filterDirection}
            onChange={(v) => setFilterDirection(v.target.value)}
          >
            <option value="=">=</option>
            <option value="<">&lt;</option>
            <option value=">">&gt;</option>
          </select>
          <input
            className="border border-gray p-2 w-24"
            onChange={(v) => setFilterNumber(v.target.value)}
          />
          <button
            className="px-4 py-2 border border-black rounded bg-blue-300 hover:bg-blue-500 duration-200"
            onClick={() => {
              setFilterRequest((f) => f + 1);
            }}
          >
            Filter
          </button>
        </div>
        <div className="flex flex-row items-center gap-2">
          <p className="font-bold">Sort by:</p>
          <select
            className="form-select w-72"
            value={sort}
            onChange={(v) => setSort(v.target.value as unknown as number)}
          >
            <option value={0}>Select...</option>
            <option value={1}>Median Age</option>
            <option value={2}>Male Population</option>
            <option value={3}>Female Population</option>
            <option value={4}>Total Population</option>
            <option value={5}>Number of Veterans</option>
          </select>
          <select
            className="form-select w-36"
            value={sortDirection}
            onChange={(v) => setSortDirection(v.target.value)}
          >
            <option value={""}>Ascending</option>
            <option value={" DESC"}>Descending</option>
          </select>
          <button
            className="px-4 py-2 border border-black rounded bg-blue-300 hover:bg-blue-500 duration-200"
            onClick={() => {
              setSortRequest((s) => s + 1);
            }}
          >
            Sort
          </button>
        </div>
      </div>
      <div className="flex flex-col items-center mb-12 mt-6">
        <nav aria-label="Page Navigation">
          <ul className="pagination">
            <li className="page-item">
              <button className="page-link" onClick={getPrevious}>
                Previous
              </button>
            </li>
            <li className="page-item">
              <button className="page-link">
                {String(currentPage) + " / " + String(numPagesToPass)}
              </button>
            </li>
            <li className="page-item">
              <button className="page-link" onClick={getNext}>
                Next
              </button>
            </li>
          </ul>
        </nav>
        <p className="mt-2">{citiesCount} Results Found</p>
      </div>
      <Grid
        cards={cards.data.map((city) => ({
          id: city.id,
          imgName: city.image_link,
          title: city.city + ", TX",
          text: `Median Age: ${city.median_age}\n Male Population: ${city.male_population}\n Female Population: ${city.female_population}\n Total Population: ${city.total_population}\n Number of Veterans: ${city.number_of_veterans}\n Average Household Size: ${city.average_household_size}`, // Add other properties as needed
          instancePage: `/cities/${city.id}`,
        }))}
        toHighlight={highlight}
      />
    </>
  );
}
