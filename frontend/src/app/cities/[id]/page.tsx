'use client'

import { useState, useEffect } from 'react'
import City from "../../components/City"

export default function Page({params, searchParams}: any) {
  const [data, setData] = useState({
    american_indian_alaskan_native_population: null,
    asian_population: null,
    average_household_size: null,
    black_african_american_population: null,
    city: null,
    female_population: null,
    foreign_born: null,
    hispanic_latino_population: null,
    id: null,
    image_link: null,
    male_population: null,
    median_age: null,
    number_of_veterans: null, 
    state: null,
    total_population: null,
    white_population: null
  })
  const [isLoading, setLoading] = useState<boolean>(true)
  const [collegeData, setCollegeData] = useState<{
    id: number,
    collegeName: string,
    path: string
  }[]>([])
  const [scholarshipData, setScholarshipData] = useState<{
    id: number,
    scholarshipName: string,
    path: string
  }[]>([])

  useEffect(() => {
    setLoading(true)
    const getData = async () => {
      const response = await fetch(`${process.env.API_URL}/api/cities?id=${params.id}`)
      const data = await response.json()
      setData(data['data'][0])
    }
    
    const getColleges = async () => {
      const response = await fetch(`${process.env.API_URL}/api/city_colleges?id=${params.id}`)
      const colleges = await response.json()
      setCollegeData(colleges.data)
    }

    getData()
    getColleges()
    setLoading(false)
  }, [params.id])

  useEffect(() => {
    const getScholarships = async () => {
      const set = new Set<{ id: number, scholarshipName: string, path: string }>();
      for (let idx = 0; idx < collegeData.length; idx++) {
        const collegeId = collegeData[idx].id;
        const response = await fetch(`${process.env.API_URL}/api/college_scholarships?college_id=${collegeId}`);
        const scholarshipIdsData = await response.json();

        // Extract scholarship IDs
        const scholarshipIds = scholarshipIdsData['data'].map((scholarship: any) => scholarship['scholarship_id']);
        // Fetch scholarship details for each ID
        for (const scholarshipId of scholarshipIds) {
          const scholarshipResponse = await fetch(`${process.env.API_URL}/api/aid?id=${scholarshipId}`);
          const scholarshipData = await scholarshipResponse.json();

          const scholarshipDetails = {
            id: scholarshipId,
            scholarshipName: scholarshipData['data'][0]['name'],
            path: `/scholarships/${scholarshipId}`
          };

          set.add(scholarshipDetails);
        }
      }
      const temp: { id: number, scholarshipName: string, path: string }[] = []
      set.forEach((val1, val2, set) => temp.push(val1))
      setScholarshipData([...temp])
    }

    getScholarships();
  }, [collegeData]);
  

  if (isLoading) return <p>Loading...</p>
  if (!data) return <p>No data</p>
  const native_pop = "American Indian and Alaskan Native Population: " + (data.american_indian_alaskan_native_population ?? "N/A")
  const asian_pop = "Asian Population: " + (data.asian_population ?? "N/A")
  const african_american_pop = "Black African American Population " + (data.black_african_american_population ?? "N/A")
  const hispanic_latino_pop = "Hispanic and Latino Population: " + (data.hispanic_latino_population ?? "N/A")
  const white_pop = "White Population: " + (data.white_population ?? "N/A")
  const female_pop = "Female Population: " + (data.female_population ?? "N/A")
  const male_pop = "Male Population: " + (data.male_population ?? "N/A")
  const total_pop = "Total Population: " + (data.total_population ?? "N/A")
  const household_size = "Average Household Size: " + (data.average_household_size ?? "N/A")
  const foreign_born = "Foreign Born Population: " + (data.foreign_born ?? "N/A")
  const median_age = "Median Age: " + (data.median_age ?? "N/A")
  const veterans = "Number of Veterans: " + (data.number_of_veterans ?? "N/A")
  const demographics = [native_pop, asian_pop, african_american_pop, hispanic_latino_pop, white_pop, female_pop, male_pop, total_pop]
  const stats = [household_size, foreign_born, median_age, veterans]

  return (
    <City
      width="100%"
      imagesrc={data.image_link}
      height = "300"
      cityName={data.city}
      stats={stats}
      scholarships={scholarshipData}
      colleges={collegeData}
      demographics={demographics}
    />
  )
}