import { College } from "./college"
import { Scholarship } from "./scholarship"

export type City = {
        id: number,
        city: string,
        state: string,
        median_age: number,
        male_population: number,
        female_population: number,
        total_population: number,
        number_of_veterans: number,
        average_household_size: number,
        foreign_born: number,
        american_indian_alaskan_native_population: number,
        asian_population: number,
        black_african_american_population: number,
        hispanic_latino_population: number,
        white_population: number,
        image_link: string,
        colleges: College[],
        scholarships: Scholarship[]
}