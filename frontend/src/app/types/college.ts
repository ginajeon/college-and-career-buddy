import { Scholarship } from "./scholarship"

export type College = {
    name: string,
    average_tuition_in_state: number,
    average_tuition_out_of_state: number,
    acceptance_rate: number,
    minority_students: number,
    majors: string[],
    average_aid: number,
    aid_received_percentage: number,
    student_faculty_ratio: number
    city: number,
    scholarships: Scholarship[]
}