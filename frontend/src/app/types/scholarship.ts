import { College } from "./college";
import { City } from "./city";

export type Scholarship = {
    name: string,
    award: string,
    awardee: string,
    need_based: boolean,
    minority_focused: boolean,
    minimum_gpa: number,
    target_field_of_study: string | null,
    link: string,
    colleges: College[],
    cities: City[]
}