import React from "react";

export default async function Commits({name}) {
    const apiUrl = "https://gitlab.com/api/v4/projects/50428851/repository/commits?author=" + name + "&per_page=100";
    // Make the fetch call
    const response = await fetch(apiUrl)
    if (!response.ok)
    {
        throw new Error(`HTTP error! Status: ${response.status}`);
    }
    const json = await response.json();
    return json.length;
    
}