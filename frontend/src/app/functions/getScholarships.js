import React from 'react'

export default async function getScholarshipsInfo({name}) {
    const apiUrl = "http://127.0.0.1:5000/api/aid/?name=" + name;
    // Make the fetch call
    const response = await fetch(apiUrl)
    if (!response.ok)
    {
        throw new Error(`HTTP error! Status: ${response.status}`);
    }
    const json = await response.json();
    return json;
}