export default async function getIssues({id}) {
    const apiUrl = "https://gitlab.com/api/v4/projects/50428851/issues?author_id=" + id;
    // Make the fetch call
    const response = await fetch(apiUrl)
    if (!response.ok)
    {
        throw new Error(`HTTP error! Status: ${response.status}`);
    }
    const json = await response.json();
    return json.length;
}