'use client'

import { useState, useEffect } from 'react'
import Scholarship from "../../components/Scholarship"

export default function Page({params, searchParams}: any) {
  const [data, setData] = useState({
    award: null,
    deadline: null,
    description_or_quote: null,
    id: null,
    image_link: null,
    link: null,
    minimum_gpa: null,
    minority_focused: null,
    name: null,
    need_based: null,
    organization: null,
    target_field_of_study: null,
  })
  const [isLoading, setLoading] = useState(true)
  const [collegeIds, setCollegeIds] = useState([]); // New state for colleges
  const [cityIds, setCityIds] = useState<number[]>([]);
  
  const [collegeData, setCollegeData] = useState<{
    id: number,
    collegeName: string,
    path: string
  }[]>([])

  const [cityData, setCityData] = useState<{
    id: number,
    cityName: string,
    path: string
  }[]>([])
 
  useEffect(() => {
    fetch(`${process.env.API_URL}/api/aid?id=${params.id}`)
      .then((res) => res.json())
      .then((data) => {
        setData(data['data'][0])
        console.log(data)
        setLoading(false)
      })
  }, [params.id])

  useEffect(() => {
    setLoading(true);
    const getCollegeIds = async () => {
      const response = await fetch(`${process.env.API_URL}/api/college_scholarships?scholarship_id=${params.id}`);
      const scholarshipColleges = await response.json(); 
      
      // Extract college_ids from ScholarshipColleges
      const collegeIdsTemp = scholarshipColleges['data'].map((college: any) => college['college_id']);
      console.log(collegeIdsTemp)
      setCollegeIds(collegeIdsTemp);
      setLoading(false);
    }
    getCollegeIds();
  }, [params.id]);

  useEffect(() => {
    const getCollegeData = async(collegeIds: number[]) => {
      let collegeData = [];
      const tempCityIds = [];

      for (let idx = 0; idx < collegeIds.length; idx++) {
        const collegeId = collegeIds[idx];
        const response = await fetch(`${process.env.API_URL}/api/colleges?id=${collegeId}`)
        const colleges = await response.json()
        collegeData.push({
          id: collegeId,
          collegeName: "" + colleges['data'][0]['name'],
          path: "/colleges/" + colleges['data'][0]['id']
        })
        tempCityIds.push(colleges['data'][0]['city_id'])
      }
      console.log(collegeData);
      setCollegeData(collegeData)
      setCityIds(tempCityIds)
      setLoading(false)
    }
    getCollegeData(collegeIds);
  }, [collegeIds])

  useEffect(() => {
    const getData = async(cityIds: number[]) => {
      let cityData = [];

      for (let idx = 0; idx < cityIds.length; idx++) {
        const cityId = cityIds[idx];
        const response = await fetch(`${process.env.API_URL}/api/cities?id=${cityId}`)
        const cities = await response.json()
        cityData.push({
          id: cityId,
          cityName: "" + cities['data'][0]['city'],
          path: "/cities/" + cities['data'][0]['id']
        })
      }
      console.log(cityData);
      setCityData(cityData)
      setLoading(false)
    }
    getData(cityIds);
  }, [cityIds])

 
  if (isLoading) return <p className="m-5">Loading...</p>
  if (!data) return <p className="m-5">No data</p>

  const organization = (data.organization == null ? "N/A" : data.organization)
  const award = "Award: " + (data.award == null ? "N/A" : data.award)
  const field_of_study = "Need Based: " + (data.target_field_of_study == null ? "N/A" : data.target_field_of_study)
  const deadline = "Application Deadline: " + (data.deadline == null ? "N/A" : data.deadline)
  const min_gpa = "Minimum GPA: " + (data.minimum_gpa == null ? "N/A" : data.minimum_gpa)
  const minority_focused = "Minority Focused: " + (data.minority_focused == null ? "N/A" : data.minority_focused)
  const need_based = "Need Based: " + (data.need_based == null ? "N/A" : data.need_based)
  const stats = organization + "\n" + award + "\n" + field_of_study + "\n" + deadline + "\n" + min_gpa + "\n" + minority_focused + "\n" + need_based
 
  return (
    <Scholarship image={data.image_link} scholarshipName={data.name} stats={stats} 
        colleges={collegeData} cities={cityData} quote={data.description_or_quote} quoteSource={data.organization}/>
    // <div>
    //   {/* <p>{JSON.stringify(data)}</p> */}
    //   {/*  */}
    // </div>
  )
}