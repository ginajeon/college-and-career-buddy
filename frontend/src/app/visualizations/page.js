"use client";
import React, { useEffect, useState } from "react";
import { createClient } from "@supabase/supabase-js";
import * as d3 from "d3";

const supabase = createClient(
  "https://yqxrcgggfzohzyjxndzf.supabase.co",
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InlxeHJjZ2dnZnpvaHp5anhuZHpmIiwicm9sZSI6ImFub24iLCJpYXQiOjE2OTY4MTIzNjIsImV4cCI6MjAxMjM4ODM2Mn0.HTuWKXIM2yrsc6bVtec89TQwAvcyZk3Sbl4nYgEDUjc"
);

export default function Visualizations() {
  useEffect(() => {
    const buildVisualization = async () => {
      const { data, error } = await supabase.rpc("race_totals");

      const finalData = [
        { label: "White", value: data[0].white },
        { label: "Hispanic or Latino", value: data[0].hispanic_latino },
        {
          label: "Black or African American",
          value: data[0].black_african_american,
        },
        { label: "Asian", value: data[0].asian },
        {
          label: "American Indian and Alaskan Native",
          value: data[0].american_indian_alaskan_native,
        },
      ];

      const pie = d3.pie();
      const arcData = pie(finalData.map((entry) => entry.value));

      const arc = d3.arc().innerRadius(0).outerRadius(100);

      const svg = d3
        .select("#pie-chart-container")
        .append("svg")
        .attr("width", 700)
        .attr("height", 300);

      const g = svg.append("g").attr("transform", "translate(200,150)");

      g.selectAll("path")
        .data(arcData)
        .enter()
        .append("path")
        .attr("d", arc)
        .attr("fill", (d, i) => d3.schemeCategory10[i]);

      const legend = svg.append("g").attr("transform", "translate(320,55)");

      const legendRectSize = 18;
      const legendSpacing = 4;

      const legendItems = legend
        .selectAll(".legend-item")
        .data(finalData)
        .enter()
        .append("g")
        .attr("class", "legend-item")
        .attr(
          "transform",
          (d, i) => "translate(0," + i * (legendRectSize + legendSpacing) + ")"
        );

      legendItems
        .append("rect")
        .attr("width", legendRectSize)
        .attr("height", legendRectSize)
        .attr("fill", (d, i) => d3.schemeCategory10[i]);

      const total = finalData.reduce((acc, entry) => acc + entry.value, 0);
      legendItems
        .append("text")
        .attr("x", legendRectSize + legendSpacing)
        .attr("y", legendRectSize - legendSpacing)
        .text(
          (d, i) => `${d.label} (${((d.value / total) * 100).toFixed(2)}%)`
        );

      // title
      svg
        .append("text")
        .attr("x", 200)
        .attr("y", 20)
        .attr("text-anchor", "middle")
        .style("font-size", "16px")
        .style("font-weight", "bold")
        .text("Population Distribution by Race of All Cities");
    };

    buildVisualization();
  });

  useEffect(() => {
    const buildVisualization = async () => {
      // Fetch data from Supabase
      const { data, error } = await supabase
        .from('scholarships')
        .select('award')
        .not('award', 'is', null);

      // Extract the award values from the data
      const awardData = data.map((entry) => entry.award);

      // Set up SVG dimensions
      const width = 700;
      const height = 300;
      const margin = { top: 50, right: 30, bottom: 40, left: 30 };

      // Create SVG container
      const svg = d3
        .select('#density-plot-container')
        .append('svg')
        .attr('width', width)
        .attr('height', height);

      // Create a histogram
      const histogram = d3.histogram().domain([0, d3.max(awardData)]).thresholds(20);

      // Generate histogram bins
      const bins = histogram(awardData);

      // Normalize the counts to represent density
      const maxCount = d3.max(bins, (bin) => bin.length);
      const densityData = bins.map((bin) => ({
        x0: bin.x0,
        x1: bin.x1,
        density: bin.length / maxCount,
      }));

      // Create xScale and yScale
      const xScale = d3.scaleLinear().domain([0, d3.max(awardData)]).range([margin.left, width - margin.right]);
      const yScale = d3.scaleLinear().domain([0, 1]).range([height - margin.bottom, margin.top]);

      // Create area path for the density plot
      const area = d3
        .area()
        .x((d) => xScale((d.x0 + d.x1) / 2))
        .y0(height - margin.bottom)
        .y1((d) => yScale(d.density));

      // Plot the density area
      svg.append('path').datum(densityData).attr('class', 'area').attr('d', area);

      // Add x-axis
      const xAxis = d3.axisBottom(xScale).tickFormat((d) => `$${d}`);
      svg
        .append('g')
        .attr('transform', `translate(0,${height - margin.bottom})`)
        .call(xAxis);

      // Add y-axis
      svg.append('g').attr('transform', `translate(${margin.left},0)`).call(d3.axisLeft(yScale));

      // Add title
      svg
        .append('text')
        .attr('x', width / 2)
        .attr('y', margin.top / 2)
        .attr('text-anchor', 'middle')
        .style('font-size', '16px')
        .style('font-weight', 'bold')
        .text('Density Plot of Scholarship Awards');
    };

    buildVisualization();
  });

  useEffect(() => {
    const buildVisualization = async () => {
      const { data, error } = await supabase.rpc("tuition_distribution");
      console.log(data);
      console.log(error);
      const finalData = [
        { label: "< $10000", value: data[0].one },
        { label: "$10000 - $20000", value: data[0].two },
        {
          label: "$20000 - $30000",
          value: data[0].three,
        },
        { label: "$30000 - $40000", value: data[0].four },
        {
          label: "> $40000",
          value: data[0].five,
        },
      ];
      const labels = [
        "< $10000",
        "$10000 - $20000",
        "$20000 - $30000",
        "$30000 - $40000",
        "> $40000",
      ];

      // Set up SVG container
      const width = 700;
      const height = 300;
      const margin = { top: 50, right: 30, bottom: 30, left: 30 };
      const chartWidth = width - margin.left - margin.right;
      const chartHeight = height - margin.top - margin.bottom;

      const svg = d3
        .select("body")
        .append("svg")
        .attr("width", width)
        .attr("height", height);

      // Define scales
      const xScale = d3
        .scaleBand()
        .domain(labels)
        .range([0, chartWidth])
        .padding(0.1);

      const yScale = d3.scaleLinear().domain([0, 100]).range([chartHeight, 0]);

      // Create bars
      svg
        .selectAll("rect")
        .data(finalData)
        .enter()
        .append("rect")
        .attr("x", (d, i) => margin.left + xScale(labels[i])) // Use the label for positioning
        .attr("y", (d) => margin.top + yScale(d.value))
        .attr("width", xScale.bandwidth())
        .attr("height", (d) => chartHeight - yScale(d.value));

      svg
        .selectAll(".x-axis-label")
        .data(labels)
        .enter()
        .append("text")
        .attr("class", "x-axis-label")
        .attr("x", (d, i) => xScale(i) + xScale.bandwidth() / 2)
        .attr("y", height + 20) // Adjust the y position for better spacing
        .attr("dy", "0.7em") // Adjust the vertical alignment
        .attr("text-anchor", "middle")
        .text((d) => d);

      // Add axes
      const xAxis = d3.axisBottom(xScale); // hide tick labels
      const yAxis = d3.axisLeft(yScale); // hide tick labels

      svg
        .append("g")
        .attr(
          "transform",
          `translate(${margin.left}, ${margin.top + chartHeight})`
        )
        .call(xAxis);

      svg
        .append("g")
        .attr("transform", `translate(${margin.left}, ${margin.top})`)
        .call(yAxis);

      // Add title
      svg
        .append("text")
        .attr("class", "chart-title")
        .style("font-size", "16px")
        .style("font-weight", "bold")
        .attr("x", 30)
        .attr("y", 20)
        .text("Distribution of Average In-State Tuition Among Texas Colleges");
    };

    buildVisualization();
  });

  return (
    <div className="mt-16">
      <div id="pie-chart-container" />
      <div id="bar-chart-container" />
      <div id="density-plot-container" />
    </div>
  );
}
