'use client'

import { useState, useEffect } from 'react'
import College from "../../components/College"
 
export default function Page({params, searchParams}: any) {
  const [data, setData] = useState({
    admission_rate: null,
    aid_received_percentage: null,
    average_aid: null,
    average_tuition_in_state: null,
    average_tuition_out_of_state: null,
    student_faculty_ratio: null,
    image_link: null,
    name: null,
    url: null, 
    city_id: null,
    minority_students: null
  })
  const [isLoading, setLoading] = useState(true)
  const [cityName, setCityName] = useState("N/A")
  const [scholarshipIds, setScholarshipIds] = useState([]) // New state for scholarships
  const [scholarshipData, setScholarshipData] = useState<{
    id: number,
    scholarshipName: string,
    path: string
  }[]>([])

   // Source: https://dev.to/stlnick/useeffect-and-async-4da8
    useEffect(() => {
      setLoading(true);

      const getData = async() => {
        const response = await fetch(`${process.env.API_URL}/api/colleges?id=${params.id}`)
        const data = await response.json()
        setData(data.data[0])
      }

      const getCityName = async() => {
        const response = await fetch(`${process.env.API_URL}/api/cities?id=${data.city_id}`)
        const cityData = await response.json()
        setCityName(cityData.data[0]?.city)
      }
      
      const getScholarshipData = async () => {
        const response = await fetch(`${process.env.API_URL}/api/college_scholarships?college_id=${params.id}`);
        const collegeScholarships = await response.json(); 
        
        // Extract scholarship_ids from scholarshipData
        const scholarshipIds = collegeScholarships.data.map((scholarship: any) => scholarship['scholarship_id']);
        const temp = []
        for (let idx = 0; idx < scholarshipIds.length; idx++) {
          const scholarshipId = scholarshipIds[idx];
          const response = await fetch(`${process.env.API_URL}/api/aid?id=${scholarshipId}`)
          const scholarship = await response.json()
          temp.push({
            id: scholarshipId,
            scholarshipName: "" + scholarship.data[0].name,
            path: "/scholarships/" + scholarship.data[0].id,
          })
        }
        setScholarshipData(temp)
      }

      getData()
      getCityName()
      getScholarshipData();
      setLoading(false);
    }, [params.id, data.city_id])
  
  if (isLoading) return <p>Loading...</p>
  if (!data) return <p>No data</p>

  const admission_rate = "Admission Rate: " + (data.admission_rate == null ? "N/A" : data.admission_rate)
  const aid_received_percentage = "Aid Received Percentage: " + (data.aid_received_percentage == null ? "N/A" : data.aid_received_percentage)
  const average_aid = "Average Aid: " + (data.average_aid == null ? "N/A" : data.average_aid)
  const average_tuition_in_state = "Average Tuition In State: " + (data.average_tuition_in_state == null ? "N/A" : data.average_tuition_in_state)
  const average_tuition_out_of_state = "Average Tuition Out of State: " + (data.average_tuition_out_of_state == null ? "N/A" : data.average_tuition_out_of_state)
  const student_faculty_ratio = "Student Faculty Ratio: " + (data.student_faculty_ratio == null ? "N/A" : data.student_faculty_ratio)
  const stats = [admission_rate, aid_received_percentage, average_aid, average_tuition_in_state, average_tuition_out_of_state, student_faculty_ratio]
  
  return (
    <College
      image={data.image_link}
      schoolName={data.name}
      stats={stats}
      scholarships={scholarshipData}
      size="400"
      minority_students={data.minority_students}
      url={data.url}
      cityName={cityName}
      cityUrl={"../../cities/"+ data.city_id}
    />
  )
}
