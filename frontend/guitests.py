from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from bs4 import BeautifulSoup
import unittest

HOME_PAGE_URL = "https://www.affordable-college-buddy.me/"

class TestGui(unittest.TestCase):
    @staticmethod
    def get_page_source(url):
        options = Options()
        driver = webdriver.Chrome(options=options)
        driver.get(url)
        return BeautifulSoup(driver.page_source, 'html.parser')
    
    # Tests the title for the homepage.
    def test_home_page_heading(self):
        soup = TestGui.get_page_source(HOME_PAGE_URL)
        paragraphs = soup.find('p')

        self.assertEqual(len(paragraphs), 1)
        self.assertEqual(next(iter(paragraphs)).text, "Affordable College Buddy")

     # Tests the background for the homepage.
    def test_home_page_background(self):
        soup = TestGui.get_page_source(HOME_PAGE_URL)
        background = soup.find('div', class_='page-background-color')
        assert background is not None

    # Tests background image for the homepage
    def test_home_page_background_image(self):
        soup = TestGui.get_page_source(HOME_PAGE_URL)
        background = soup.find('div', class_='page-background-color')
        style = background.get('style')
        assert 'background-image:url(/college.jpg)' in style

    # Tests background image for the homepage
    def test_home_page_image_size(self):
        soup = TestGui.get_page_source(HOME_PAGE_URL)
        background = soup.find('div', class_='page-background-color')
        style = background.get('style')
        assert 'background-size:cover;' in style

    # Tests the title for the scholarship page
    def test_scholarships_page_heading(self):
        soup = TestGui.get_page_source("%s/%s" % (HOME_PAGE_URL, "scholarships"))
        paragraphs = soup.find('p')
        self.assertEqual(len(paragraphs), 1)
        self.assertEqual(next(iter(paragraphs)).text, "Scholarships")

    # Tests the title for the cities page
    def test_cities_page_heading(self):
        soup = TestGui.get_page_source("%s/%s" % (HOME_PAGE_URL, "cities"))
        paragraphs = soup.find('p')
        self.assertEqual(len(paragraphs), 1)
        self.assertEqual(next(iter(paragraphs)).text, "Cities")

    # Tests the title for the colleges page
    def test_colleges_page_heading(self):
        soup = TestGui.get_page_source("%s/%s" % (HOME_PAGE_URL, "colleges"))
        paragraphs = soup.find('p')
        self.assertEqual(len(paragraphs), 1)
        self.assertEqual(next(iter(paragraphs)).text, "Colleges")

    # Tests the title for the about page
    def test_about_page_heading(self):
        soup = TestGui.get_page_source("%s/%s" % (HOME_PAGE_URL, "about"))
        paragraphs = soup.find('p')
        self.assertEqual(len(paragraphs), 1)
        self.assertEqual(next(iter(paragraphs)).text, "About")

    # Tests the paragraph description for the about page
    def test_about_page_description(self):
        soup = TestGui.get_page_source("%s/%s" % (HOME_PAGE_URL, "about"))
        paragraphs = soup.find('p', class_='mx-[20%]')
        self.assertEqual(len(paragraphs), 1)
        self.assertEqual(next(iter(paragraphs)).text, "Affordable College Buddy was created to support low-income high school students in Austin, TX who don't have access to college counseling. We provide students with information on affordable colleges close to their homes, scholarship options to fund their education, and information about the different cities.")

    # Tests the headers for the about page
    def test_about_page_headers(self):
        soup = TestGui.get_page_source("%s/%s" % (HOME_PAGE_URL, "about"))
        header1 = soup.find(string="Team")
        self.assertEqual(header1, "Team")
        header2 = soup.find(string="Tools Used")
        self.assertEqual(header2, "Tools Used")
        header3 = soup.find(string="Data Sources")
        self.assertEqual(header3, "Data Sources")
        header4 = soup.find(string="Interesting Result of Integrating Disparate Data")
        self.assertEqual(header4, "Interesting Result of Integrating Disparate Data")

if __name__ == "__main__":
    unittest.main()